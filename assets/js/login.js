let API_URL         = sessionStorage.getItem("api-url")
let WEB_URL         = sessionStorage.getItem("cms-url")
let CONFIG_API_URL  = `${API_URL}config/`

let TOAST           = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000
                      });
let SESSION         = sessionStorage.getItem("user-token");
let ADMIN_ROLE      = sessionStorage.getItem("user-role");

$(document).ready(function(){
  if(SESSION){ 
    // if(ADMIN_ROLE == 'SUPERADMIN') window.location.href = 'dashboard'
    // else if(ADMIN_ROLE == 'SALES') window.location.href = 'sales'
    // else window.location.href = 'outbounds'
    getProfile()
  }else{
    getConfig()
  }

  $('#password-visibility').click(function(){
    var x = document.getElementById("login-password-field");
    if (x.type === "password") {
      x.type = "text";
      $('#visibility-checkbox').prop('checked', true); // Checks it
    } else {
      x.type = "password";
      $('#visibility-checkbox').prop('checked', false); // Checks it
    }
  })

  $("#login-form").submit(function(e) {
    e.preventDefault();
    startLoadingButton("#login-button")
  
    let $form = $( this ),
        username = $form.find( "input[name='username']" ).val(),
        password = $form.find( "input[name='password']" ).val()
    
    $.ajax({
        async: true,
        url: `${API_URL}admin/login`,
        type: 'POST',
        data: JSON.stringify({
          username: username,
          password: password
        }),
        error: function(res) {
          response = res.responseJSON
          showError(response.message)
          endLoadingButton('#login-button', 'Log In')
        },
        success: function(res) {
          response = res.data;
          setSession(response)
          // if(ADMIN_ROLE == 'SUPERADMIN') window.location.href = 'dashboard'
          // else if(ADMIN_ROLE == 'ADMIN') window.location.href = 'outbounds'
          // else if(ADMIN_ROLE == 'SALES') window.location.href = 'sales-books'
          window.location.href = 'dashboard'
        }
    });    
  })
})

function getProfile(){
  $.ajax({
    async: true,
    url: `${API_URL}admin/profile`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      response = res.responseJSON
      // removeSession()
    },
    success: function(res) {
      response = res.data;
      setSession(response)
      window.location.href = 'dashboard'
    }
  }); 
}


function getConfig(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let config = res.data
      if(config){
        if(config.title_1){
          $('#login-title').html(config.title_1)
        }
        if(config.logo_1_url){
          $('#login-logo').prop("src", API_URL+config.logo_1_url)
        }
        if(config.icon_url){
          $("#favicon").attr("href", API_URL+config.icon_url);
        }
      }
    }
  });
}

function removeSession(){
  console.log("REMOVE SESSION")
  sessionStorage.removeItem("user-token");
  sessionStorage.removeItem("user-refresh-token");
  sessionStorage.removeItem("user-fullname");
  sessionStorage.removeItem("user-role");
  window.location.href = `${WEB_URL}login`
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  console.log("SET NEW SESSION")
  sessionStorage.setItem("user-token", data.access_token)
  sessionStorage.setItem("user-refresh-token", data.refresh_token)
  sessionStorage.setItem("user-fullname", data.fullname);
  sessionStorage.setItem("user-role", data.role);
  SESSION = data.access_token
  REFRESH_SESSION = data.refresh_token
  ADMIN_FULLNAME = data.fullname
  ADMIN_ROLE = data.role
}