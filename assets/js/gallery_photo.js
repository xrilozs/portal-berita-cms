let PHOTO_ID

$(document).ready(function(){

  //render datatable
  let photo_table = $('#photo-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: PHOTO_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "title",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "alias",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          {
            data: "full_thumbnail_url",
            render: function (data, type, row, meta) {
              return `<img src="${data}" style="width:100px;">`
            },
          },
          {
            data: "description",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "status",
            orderable: false,
            render: function (data, type, row, meta) {
              let color = data == 'DRAFT' ? 'default' : 'primary'
              let badge = `<span class="badge badge-pill badge-${color}">${data}</span>`

              return badge
            }
          },
          {
            data: "event_date",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">`

                actions += `<a href="${WEB_URL}gallery-photo/update/${row.alias}" class="btn btn-sm btn-success" title="update">
                  <i class="fas fa-edit"></i>
                </a>
                <button class="btn btn-sm btn-danger photo-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#photo-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`

                if(row.status == 'DRAFT'){
                  actions += `<button class="btn btn-sm btn-primary photo-publish-toggle" data-id="${data}" data-toggle="modal" data-target="#photo-publish-modal" title="publikasi">
                    <i class="fas fa-check"></i>
                  </button>`
                }else{
                  actions += `<button class="btn btn-sm btn-warning photo-unpublish-toggle" data-id="${data}" data-toggle="modal" data-target="#photo-unpublish-modal" title="draft">
                    <i class="fas fa-ban"></i>
                  </button>`
                }

                actions += '</div>'

                return actions
            }
          }
      ]
  });
  
  $("body").delegate(".photo-delete-toggle", "click", function(e) {
    PHOTO_ID = $(this).data('id')
    console.log("ID:", PHOTO_ID)
  })

  $("body").delegate(".photo-publish-toggle", "click", function(e) {
    PHOTO_ID = $(this).data('id')
    console.log("ID:", PHOTO_ID)
  })

  $("body").delegate(".photo-unpublish-toggle", "click", function(e) {
    PHOTO_ID = $(this).data('id')
    console.log("ID:", PHOTO_ID)
  })

  //submit form  
  $('#photo-delete-button').click(function (){
    startLoadingButton('#photo-delete-button')
    
    $.ajax({
        async: true,
        url: `${PHOTO_API_URL}${PHOTO_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#photo-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#photo-delete-button', 'Iya')
          showSuccess(res.message)
          $('#photo-delete-modal').modal('toggle')
          photo_table.ajax.reload()
        }
    });
  })

  $('#photo-publish-button').click(function (){
    startLoadingButton('#photo-publish-button')
    
    $.ajax({
        async: true,
        url: `${PHOTO_API_URL}publish/${PHOTO_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#photo-publish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#photo-publish-button', 'Iya')
          showSuccess(res.message)
          $('#photo-publish-modal').modal('toggle')
          photo_table.ajax.reload()
        }
    });
  })

  $('#photo-unpublish-button').click(function (){
    startLoadingButton('#photo-unpublish-button')
    
    $.ajax({
        async: true,
        url: `${PHOTO_API_URL}unpublish/${PHOTO_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#photo-unpublish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#photo-unpublish-button', 'Iya')
          showSuccess(res.message)
          $('#photo-unpublish-modal').modal('toggle')
          photo_table.ajax.reload()
        }
    });
  })

  $('#photo-title-update-field').change(function(){
    let val = $(this).val()
    val = val.trim()
    let alias = val.toLowerCase().replace(" ", "-")
    $('#photo-alias-update-field').val(alias)
  })  
})
