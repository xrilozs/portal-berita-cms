let NEWS_ID,
    IMG_URL,
    FULL_IMG_URL

$(document).ready(function(){
  getCategoryNews();

  if(ADMIN_ROLE == 'USER'){
    $('#news-status-create').hide()
  }

  //render datatable
  let news_table = $('#news-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: NEWS_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "title",
            render: function(data){
              return sortText(data)
            }
          },
          { 
            data: "category_news_name",
          },
          { 
            data: "alias",
            render: function(data){
              return sortText(data)
            }
          },
          {
            data: "full_img_url",
            render: function (data, type, row, meta) {
              return `<img src="${data}" class="img-fluid" style="max-width:100px;">`
            },
          },
          {
            data: "meta_description",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          {
            data: "meta_keywords",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "status",
            orderable: false,
            render: function (data, type, row, meta) {
              let color = data == 'DRAFT' ? 'default' : 'primary'
              let badge = `<span class="badge badge-pill badge-${color}">${data}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">`

                actions += `<button class="btn btn-sm btn-success news-update-toggle" data-id="${data}" data-toggle="modal" data-target="#news-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger news-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#news-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`

                if(ADMIN_ROLE != 'USER'){
                  if(row.status == 'DRAFT'){
                    actions += `<button class="btn btn-sm btn-primary news-publish-toggle" data-id="${data}" data-toggle="modal" data-target="#news-publish-modal" title="publish">
                      <i class="fas fa-check"></i>
                    </button>`
                  }else{
                    actions += `<button class="btn btn-sm btn-warning news-unpublish-toggle" data-id="${data}" data-toggle="modal" data-target="#news-unpublish-modal" title="draft">
                      <i class="fas fa-ban"></i>
                    </button>`
                  }
                }

                actions += '</div>'

                return actions
            },
          }
      ]
  });
  
  //button action click
  $("#news-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".news-update-toggle", "click", function(e) {
    NEWS_ID = $(this).data('id')
    clearForm('update')

    $('#news-update-overlay').show()
    $.ajax({
        async: true,
        url: `${NEWS_API_URL}by-id/${NEWS_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#news-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          IMG_URL = response.img_url
          FULL_IMG_URL = response.full_img_url
          $('#news-update-overlay').hide()
          renderImage()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".news-delete-toggle", "click", function(e) {
    NEWS_ID = $(this).data('id')
    console.log("ID:", NEWS_ID)
  })

  $("body").delegate(".news-publish-toggle", "click", function(e) {
    NEWS_ID = $(this).data('id')
    console.log("ID:", NEWS_ID)
  })

  $("body").delegate(".news-unpublish-toggle", "click", function(e) {
    NEWS_ID = $(this).data('id')
    console.log("ID:", NEWS_ID)
  })

  //upload image
  $('#news-uploadImage-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#news-uploadImage-button')
    
    $.ajax({
        async: true,
        url: `${NEWS_API_URL}upload-image`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#news-uploadImage-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#news-uploadImage-button', 'Upload')
        },
        success: function(res) {
          const response = res.data
          IMG_URL = response.img_url
          FULL_IMG_URL = response.full_img_url
          endLoadingButton('#news-uploadImage-button', 'Upload')
          showSuccess(res.message)
          renderImage()
          $('#news-uploadImage-modal').modal('hide')
        }
    });
  });

  //submit form
  $('#news-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#news-create-button')
    
    let $form = $(this)
    let data = {
      title:  $form.find( "input[name='title']" ).val(),
      content: $('.rich-text-create').summernote('code'),
      status: $('#news-status-create-field').find(":selected").val(),
      category_news_id: $('#news-category-create-field').find(":selected").val(),
      img_url: IMG_URL,
      meta_description: $form.find( "textarea[name='meta_description']" ).val(),
      meta_keywords: $form.find( "textarea[name='meta_keywords']" ).val()
    }
    $.ajax({
        async: true,
        url: NEWS_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#news-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#news-create-button', 'Simpan')
          showSuccess(res.message)
          $('#news-create-modal').modal('hide')
          news_table.ajax.reload()
        }
    });
  })
  
  $('#news-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#news-update-button')

    let $form = $(this)
    let data = {
      id: NEWS_ID,
      title:  $form.find( "input[name='title']" ).val(),
      content: $('.rich-text-update').summernote('code'),
      status: $('#news-status-update-field').find(":selected").val(),
      category_news_id: $('#news-category-update-field').find(":selected").val(),
      img_url: IMG_URL,
      meta_description: $form.find( "textarea[name='meta_description']" ).val(),
      meta_keywords: $form.find( "textarea[name='meta_keywords']" ).val()
    }
    console.log("REQUEST: ", data)
    $.ajax({
        async: true,
        url: NEWS_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#news-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#news-update-button', 'Simpan')
          showSuccess(res.message)
          $('#news-update-modal').modal('toggle')
          news_table.ajax.reload()
        }
    });
  })
  
  $('#news-delete-button').click(function (){
    startLoadingButton('#news-delete-button')
    
    $.ajax({
        async: true,
        url: `${NEWS_API_URL}${NEWS_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#news-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#news-delete-button', 'Iya')
          showSuccess(res.message)
          $('#news-delete-modal').modal('toggle')
          news_table.ajax.reload()
        }
    });
  })

  $('#news-publish-button').click(function (){
    startLoadingButton('#news-publish-button')
    
    $.ajax({
        async: true,
        url: `${NEWS_API_URL}publish/${NEWS_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#news-publish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#news-publish-button', 'Iya')
          showSuccess(res.message)
          $('#news-publish-modal').modal('toggle')
          news_table.ajax.reload()
        }
    });
  })

  $('#news-unpublish-button').click(function (){
    startLoadingButton('#news-unpublish-button')
    
    $.ajax({
        async: true,
        url: `${NEWS_API_URL}unpublish/${NEWS_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#news-unpublish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#news-unpublish-button', 'Iya')
          showSuccess(res.message)
          $('#news-unpublish-modal').modal('toggle')
          news_table.ajax.reload()
        }
    });
  })

  $('#news-title-update-field').change(function(){
    let val = $(this).val()
    val = val.trim()
    let alias = val.toLowerCase().replace(" ", "-")
    $('#news-alias-update-field').val(alias)
  })  
})


function getCategoryNews(){
  $.ajax({
    async: true,
    url: `${CATEGORY_NEWS_API_URL}all`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      renderCategoryOption(response)
    }
  });
}

function renderCategoryOption(data){
  let options_html = ``
  for(let item of data){
    const option_html = `<option value="${item.id}">${item.name}</option>`
    options_html += option_html
  }

  $('.news-category-option').html(options_html)
}

function renderForm(data, type){
  let $form = $(`#news-${type}-form`)
  $form.find( "input[name='title']" ).val(data.title)
  $(`#news-status-${type}-field`).val(data.status).change()
  $(`#news-category-${type}-field`).val(data.category_news_id).change()
  $(`.rich-text-${type}`).summernote("code", data.content)
  if(type == "update"){
    $form.find( "input[name='alias']" ).val(data.alias)
  }
  $form.find( "textarea[name='meta_description']" ).val(data.meta_description),
  $form.find( "textarea[name='meta_keywords']" ).val(data.meta_keywords)
  
  if(ADMIN_ROLE == 'USER'){
    $('#news-status-update-field').attr("disabled", true)
  }
}

function clearForm(type){
  let $form = $(`#news-${type}-form`)
  $form.find( "input[name='title']" ).val("")
  $(`.rich-text-${type}`).summernote("code", "")
}

function renderImage(){
  let img_html = `<img src="${FULL_IMG_URL}" class="img-fluid" style="max-width:100px;">`
  $('.news-displayImage').html(img_html)
}
