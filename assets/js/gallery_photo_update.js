let THUMBNAIL_URL = null,
    LIST_PHOTO = [],
    ALIAS = window.location.pathname.split("/").pop(),
    ID = null,
    ITEM_ID = null

$(document).ready(function(){
  get_photo_detail()

  $('#thumbnail-photo-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#thumbnail-photo-button')
    
    $.ajax({
        async: true,
        url: `${PHOTO_API_URL}upload-image`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#thumbnail-photo-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#thumbnail-photo-button', 'Simpan')
        },
        success: function(res) {
          const response = res.data
          THUMBNAIL_URL = response.img_url
          endLoadingButton('#thumbnail-photo-button', 'Simpan')
          $('#thumbnail-photo-modal').modal('hide')
          showSuccess(res.message)
          render_thumbnail()
        }
    });
  });

  $('#add-photo-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#add-photo-button')
    
    $.ajax({
        async: true,
        url: `${PHOTO_API_URL}upload-image`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#add-photo-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#add-photo-button', 'OK')
        },
        success: function(res) {
          upload_photo_item(res.data.img_url)
        }
    });
  });

  $('#update-photo-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#update-photo-button')

    let isValidThumbnail  = THUMBNAIL_URL ? true : false

    if(!isValidThumbnail){
      showError('Thumbnail belum diisi')
      endLoadingButton('#update-photo-button', 'Simpan')
    }else{
      let $form = $(this)
      let data = {
        id: ID,
        title:  $form.find( "input[name='title']" ).val(),
        description: $form.find( "textarea[name='description']" ).val(),
        status: $('#photo-status-update-field').find(":selected").val(),
        thumbnail_url: THUMBNAIL_URL,
        event_date: $form.find( "input[name='event_date']" ).val()
      }
      $.ajax({
          async: true,
          url: PHOTO_API_URL,
          type: 'PUT',
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          data: JSON.stringify(data),
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#update-photo-button', 'Simpan')
          },
          success: function(res) {
            endLoadingButton('#update-photo-button', 'Simpan')
            showSuccessV2("Update Photo", "Update Photo Success", `${WEB_URL}gallery-photo`)
          }
      });
    }
  })

  $("body").delegate(".delete-photo", "click", function(e) {
    let id = $(this).data('id')
    ITEM_ID = id
  })

  $('#delete-photo-button').click(function(){
    $.ajax({
      async: true,
      url: `${PHOTO_API_URL}item/${ITEM_ID}`,
      type: 'DELETE',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#delete-photo-button', 'OK')
      },
      success: function(res) {
        endLoadingButton('#delete-photo-button', 'OK')
        $('#delete-photo-modal').modal('hide')

        showSuccess("Berhasil menghapus foto")
        get_photo_detail()
      }
    });
  })
})

function get_photo_detail(){
  $.ajax({
    async: true,
    url: `${PHOTO_API_URL}by-alias/${ALIAS}`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      ID = res.data.id
      THUMBNAIL_URL = res.data.thumbnail_url
      LIST_PHOTO = res.data.items
      render_form(res.data)
      render_photos()
      render_thumbnail()
    }
  });
}

function upload_photo_item(url){
    let data = {
      gallery_photo_id: ID,
      photo_url: url
    }

    $.ajax({
      async: true,
      url: `${PHOTO_API_URL}item`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#add-photo-button', 'OK')
      },
      success: function(res) {
        endLoadingButton('#add-photo-button', 'OK')
        $('#add-photo-modal').modal('hide')

        showSuccess("Berhasil menambahkan foto")
        get_photo_detail()
      }
    });
}

function render_form(data){
  let $form = $(`#update-photo-form`)
  $form.find( "input[name='title']" ).val(data.title)
  $(`#photo-status-update-field`).val(data.status).change()
  $form.find( "textarea[name='description']" ).val(data.description),
  // $form.find( "textarea[name='evemt_date']" ).val(data.event_date)
  $('.datepicker').datepicker("setDate", new Date(data.event_date));
}

function render_thumbnail(){
  let img_html = `<img src="${API_URL}${THUMBNAIL_URL}" class="img-fluid" style="max-width:100px;">`
  $('.photo-display-thumbnail').html(img_html)
}

function render_photos(){
  let photos_html = ""
  LIST_PHOTO.forEach((item) => {
    const item_html = `<div class="col-lg-2 col-md-3 col-sm-6 mb-2">
      <div class="row">
        <div class="col-12 mb-4 d-flex justify-content-center">
          <img src="${API_URL}${item.photo_url}" class="img-fluid" style="max-width:100px;">
        </div>
        <div class="col-12 d-flex justify-content-center">
          <button type="button" class="btn btn-sm btn-danger delete-photo" data-id="${item.id}" data-toggle="modal" data-target="#delete-photo-modal">
            <i class="fas fa-trash"></i> Hapus
          </button>
        </div>
      </div>
    </div>`
    photos_html += item_html
  });

  $('#list-photos').html(photos_html)
}
