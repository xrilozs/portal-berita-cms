let DOCUMENT_ID,
    DOCUMENT_URL,
    FULL_DOCUMENT_URL,
    DOCUMENT_FILE_SIZE

$(document).ready(function(){
  //render datatable
  let document_table = $('#document-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: DOCUMENT_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name",
            render: function(data){
              return sortText(data)
            }
          },
          { 
            data: "alias",
            render: function(data){
              return sortText(data)
            }
          },
          {
            data: "full_document_url",
            render: function (data, type, row, meta) {
              return `<a href="${data}" target="_blank">${data}</a>`
            },
          },
          {
            data: "size"
          },
          { 
            data: "status",
            orderable: false,
            render: function (data, type, row, meta) {
              let color = data == 'DRAFT' ? 'default' : 'primary'
              let badge = `<span class="badge badge-pill badge-${color}">${data}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">`

                actions += `<button class="btn btn-sm btn-success document-update-toggle" data-id="${data}" data-toggle="modal" data-target="#document-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger document-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#document-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`

                if(row.status == 'DRAFT'){
                  actions += `<button class="btn btn-sm btn-primary document-publish-toggle" data-id="${data}" data-toggle="modal" data-target="#document-publish-modal" title="publish">
                    <i class="fas fa-check"></i>
                  </button>`
                }else{
                  actions += `<button class="btn btn-sm btn-warning document-unpublish-toggle" data-id="${data}" data-toggle="modal" data-target="#document-unpublish-modal" title="draft">
                    <i class="fas fa-ban"></i>
                  </button>`
                }

                actions += '</div>'

                return actions
            },
          }
      ]
  });
  
  //button action click
  $("#document-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".document-update-toggle", "click", function(e) {
    DOCUMENT_ID = $(this).data('id')
    clearForm('update')

    $('#document-update-overlay').show()
    $.ajax({
        async: true,
        url: `${DOCUMENT_API_URL}by-id/${DOCUMENT_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#document-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          DOCUMENT_URL = response.document_url
          FULL_DOCUMENT_URL = response.full_document_url
          DOCUMENT_FILE_SIZE = response.size
          $('#document-update-overlay').hide()
          renderDocument()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".document-delete-toggle", "click", function(e) {
    DOCUMENT_ID = $(this).data('id')
    console.log("ID:", DOCUMENT_ID)
  })

  $("body").delegate(".document-publish-toggle", "click", function(e) {
    DOCUMENT_ID = $(this).data('id')
    console.log("ID:", DOCUMENT_ID)
  })

  $("body").delegate(".document-unpublish-toggle", "click", function(e) {
    DOCUMENT_ID = $(this).data('id')
    console.log("ID:", DOCUMENT_ID)
  })

  //upload image
  $('#upload-document-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#upload-document-button')
    
    $.ajax({
        async: true,
        url: `${DOCUMENT_API_URL}upload-document`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#upload-document-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#upload-document-button', 'Simpan')
        },
        success: function(res) {
          const response = res.data
          DOCUMENT_URL = response.document_url
          FULL_DOCUMENT_URL = response.full_document_url
          DOCUMENT_FILE_SIZE = response.size
          endLoadingButton('#upload-document-button', 'Simpan')
          showSuccess(res.message)
          renderDocument()
          $('#upload-document-modal').modal('hide')
        }
    });
  });

  //submit form
  $('#document-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#document-create-button')
    
    let $form = $(this)
    let data = {
      name:  $form.find( "input[name='name']" ).val(),
      status: $('#document-status-create-field').find(":selected").val(),
      document_url: DOCUMENT_URL,
      size: DOCUMENT_FILE_SIZE
    }
    $.ajax({
        async: true,
        url: DOCUMENT_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#document-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#document-create-button', 'Simpan')
          showSuccess(res.message)
          $('#document-create-modal').modal('hide')
          document_table.ajax.reload()
        }
    });
  })
  
  $('#document-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#document-update-button')

    let $form = $(this)
    let data = {
      id: DOCUMENT_ID,
      name:  $form.find( "input[name='name']" ).val(),
      status: $('#document-status-update-field').find(":selected").val(),
      document_url: DOCUMENT_URL,
      size: DOCUMENT_FILE_SIZE
    }
    console.log("REQUEST: ", data)
    $.ajax({
        async: true,
        url: DOCUMENT_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#document-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#document-update-button', 'Simpan')
          showSuccess(res.message)
          $('#document-update-modal').modal('toggle')
          document_table.ajax.reload()
        }
    });
  })
  
  $('#document-delete-button').click(function (){
    startLoadingButton('#document-delete-button')
    
    $.ajax({
        async: true,
        url: `${DOCUMENT_API_URL}${DOCUMENT_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#document-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#document-delete-button', 'Iya')
          showSuccess(res.message)
          $('#document-delete-modal').modal('toggle')
          document_table.ajax.reload()
        }
    });
  })

  $('#document-publish-button').click(function (){
    startLoadingButton('#document-publish-button')
    
    $.ajax({
        async: true,
        url: `${DOCUMENT_API_URL}publish/${DOCUMENT_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#document-publish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#document-publish-button', 'Iya')
          showSuccess(res.message)
          $('#document-publish-modal').modal('toggle')
          document_table.ajax.reload()
        }
    });
  })

  $('#document-unpublish-button').click(function (){
    startLoadingButton('#document-unpublish-button')
    
    $.ajax({
        async: true,
        url: `${DOCUMENT_API_URL}unpublish/${DOCUMENT_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#document-unpublish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#document-unpublish-button', 'Iya')
          showSuccess(res.message)
          $('#document-unpublish-modal').modal('toggle')
          document_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#document-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $(`#document-status-${type}-field`).val(data.status).change()
}

function clearForm(type){
  let $form = $(`#document-${type}-form`)
  $form.find( "input[name='name']" ).val("")
}

function renderDocument(){
  let img_html = `<a href="${FULL_DOCUMENT_URL}" target="_blank">${FULL_DOCUMENT_URL}</a><br>Ukuran: ${DOCUMENT_FILE_SIZE}`
  $('.display-document').html(img_html)
}
