let CONFIG_ID;
let LOGO_1_URL;
let LOGO_2_URL;
let FULL_LOGO_1_URL;
let FULL_LOGO_2_URL;
let ICON_URL;
let FULL_ICON_URL;
let LOGO_ID;

$(document).ready(function(){
  getWebConfig()
  
  $('#contact-form').submit(function (e){
    e.preventDefault();
    startLoadingButton("#contact-button")
    
    let $form = $( this ),
        request = {
          id: CONFIG_ID,
          google_map: $form.find( "textarea[name='google_map']" ).val(),
          email: $form.find( "input[name='email']" ).val(),
          phone: $form.find( "input[name='phone']" ).val(),
          address: $form.find( "textarea[name='address']" ).val()
        }

    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}contact`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#contact-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#contact-button', 'Simpan')
          showSuccess(res.message)
          getWebConfig()
        }
    });
  })
  
  $('#social-form').submit(function (e){
    e.preventDefault();
    startLoadingButton("#social-button")
    
    let $form = $( this ),
        request = {
          id: CONFIG_ID,
          sm_facebook: $form.find( "input[name='sm_facebook']" ).val(),
          sm_twitter: $form.find( "input[name='sm_twitter']" ).val(),
          sm_instagram: $form.find( "input[name='sm_instagram']" ).val(),
          sm_youtube: $form.find( "input[name='sm_youtube']" ).val(),
        }

    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}social`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#social-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#social-button', 'Simpan')
          showSuccess(res.message)
          getWebConfig()
        }
    });
  })

  $('#web-form').submit(function (e){
    e.preventDefault();
    startLoadingButton("#web-button")
    
    let $form = $( this ),
        request = {
          id: CONFIG_ID,
          title_1: $form.find( "input[name='title_1']" ).val(),
          title_2: $form.find( "input[name='title_2']" ).val(),
          logo_1_url: LOGO_1_URL,
          logo_2_url: LOGO_2_URL,
          icon_url: ICON_URL,
        }

    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}web`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#web-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#web-button', 'Simpan')
          showSuccess(res.message)
          getWebConfig()
        }
    });
  })

  $('.config-upload-logo').click(function(){
    LOGO_ID = $(this).data('id')
    console.log(LOGO_ID)
  })
})

function getWebConfig(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let config = res.data
      if(config){
        CONFIG_ID = res.data.id
        renderContactForm(res.data)
        renderSocialForm(res.data)
        renderWebForm(res.data)
      }
      $('#contact-overlay').hide()
      $('#social-overlay').hide()
      $('#web-overlay').hide()
    }
  });
}

function renderContactForm(data){
  let $form = $(`#contact-form`)
  $form.find( "textarea[name='address']" ).val(data.address)
  $form.find( "input[name='email']" ).val(data.email)
  $form.find( "input[name='phone']" ).val(data.phone)
  $form.find( "textarea[name='google_map']" ).val(data.google_map)
}

function renderSocialForm(data){
  let $form = $(`#social-form`)
  $form.find( "input[name='sm_facebook']" ).val(data.sm_facebook)
  $form.find( "input[name='sm_twitter']" ).val(data.sm_twitter)
  $form.find( "input[name='sm_instagram']" ).val(data.sm_instagram)
  $form.find( "input[name='sm_youtube']" ).val(data.sm_youtube)
}

function renderWebForm(data){
  let $form = $(`#web-form`)
  $form.find( "input[name='title_1']" ).val(data.title_1)
  $form.find( "input[name='title_2']" ).val(data.title_2)
  LOGO_1_URL = data.logo_1_url
  LOGO_2_URL = data.logo_2_url
  ICON_URL = data.icon_url
  if(LOGO_1_URL){
    let logo_html = `<img src="${API_URL + LOGO_1_URL}" class="img-fluid" style="height:100px;">`
    $(`#config-display-logo-1`).html(logo_html)
  }
  if(LOGO_2_URL){
    let logo_html = `<img src="${API_URL + LOGO_2_URL}" class="img-fluid" style="height:100px;">`
    $(`#config-display-logo-2`).html(logo_html)
  }
  if(ICON_URL){
    let icon_html = `<img src="${API_URL + ICON_URL}" class="img-fluid" style="height:100px;">`
    $(`#config-display-icon`).html(icon_html)
  }
}

//upload image
$('#config-upload-logo-form').submit(function(e){
  e.preventDefault()
  startLoadingButton('#config-upload-logo-button')
  
  $.ajax({
      async: true,
      url: `${CONFIG_API_URL}upload-image`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: new FormData($('#config-upload-logo-form')[0]),
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#config-upload-logo-button', 'OK')
      },
      success: function(res) {
        const response = res.data
        if(LOGO_ID == 1){
          LOGO_1_URL = response.img_url
          FULL_LOGO_1_URL = response.full_img_url
        }else if(LOGO_ID == 2){
          LOGO_2_URL = response.img_url
          FULL_LOGO_2_URL = response.full_img_url
        }
        
        endLoadingButton('#config-upload-logo-button', 'OK')
        showSuccess(res.message)
        renderImage(`logo-${LOGO_ID}`)
        $('#config-upload-logo-modal').modal('hide')
      }
  });
});

$('#config-upload-icon-form').submit(function(e){
  e.preventDefault()
  startLoadingButton('#config-upload-icon-button')
  
  $.ajax({
      async: true,
      url: `${CONFIG_API_URL}upload-image`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: new FormData($('#config-upload-icon-form')[0]),
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('#config-upload-icon-button', 'Submit')
      },
      success: function(res) {
        const response = res.data
        ICON_URL = response.img_url
        FULL_ICON_URL = response.full_img_url
        endLoadingButton('#config-upload-icon-button', 'Submit')
        showSuccess(res.message)
        renderImage('icon')
        $('#config-upload-icon-modal').modal('hide')
      }
  });
});

function renderImage(type){
  if(type == 'logo-1'){
    let img_html = `<img src="${FULL_LOGO_1_URL}" class="img-fluid" style="max-width:100px;">`
    $(`#config-display-logo-1`).html(img_html)
  }else if(type == 'logo-2'){
    let img_html = `<img src="${FULL_LOGO_2_URL}" class="img-fluid" style="max-width:100px;">`
    $(`#config-display-logo-2`).html(img_html)
  }else if(type == 'icon'){
    let img_html = `<img src="${FULL_ICON_URL}" class="img-fluid" style="max-width:100px;">`
    $(`#config-display-icon`).html(img_html)
  }
}
