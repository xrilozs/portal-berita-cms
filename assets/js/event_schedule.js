let SCHEDULE_ID,
    DOCUMENT_URL,
    FULL_DOCUMENT_URL

$(document).ready(function(){
  get_event()

  //render datatable
  let schedule_table = $('#event-schedule-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: SCHEDULE_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "event_nama",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "description",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "event_status",
            orderable: false,
            render: function (data, type, row, meta) {
              let text = parseInt(data) ? 'ACTIVE' : 'NONACTIVE'
              let color = parseInt(data) ? 'primary' : 'danger'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            }
          },
          {
            data: "event_tgl_awal",
            render: function(data, type, row){
              return `${formatDateID(data)} - ${formatDateID(row.event_tgl_akhir)}`
            }
          },

          {
            data: "pdf_url",
            render: function(data, type, row){
              return `<a href="$data" target="_blank">${data}</a>`
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">
                  <button class="btn btn-sm btn-success event-schedule-update-toggle" data-id="${data}" data-toggle="modal" data-target="#event-schedule-update-modal" title="update">
                    <i class="fas fa-edit"></i>
                  </button>
                  <button class="btn btn-sm btn-danger event-schedule-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#event-schedule-delete-modal" title="hapus">
                    <i class="fas fa-trash"></i>
                  </button>
                </div>`

                return actions
            }
          }
      ]
  });
  
  $("body").delegate(".event-schedule-delete-toggle", "click", function(e) {
    SCHEDULE_ID = $(this).data('id')
    console.log("ID:", SCHEDULE_ID)
  })

  $("body").delegate(".event-schedule-update-toggle", "click", function(e) {
    SCHEDULE_ID = $(this).data('id')
    console.log("ID:", SCHEDULE_ID)

    clearForm('update')

    $('#event-schedule-update-overlay').show()
    $.ajax({
        async: true,
        url: `${SCHEDULE_API_URL}by-id/${SCHEDULE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#event-schedule-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          DOCUMENT_URL = response.pdf_url
          FULL_DOCUMENT_URL = response.full_pdf_url
          $('#event-schedule-update-overlay').hide()
          renderDocument()
          renderForm(response, 'update')
        }
    });
  })

  //submit form
  $('#upload-document-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#upload-document-button')
    
    $.ajax({
        async: true,
        url: `${SCHEDULE_API_URL}upload-document`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#upload-document-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#upload-document-button', 'Simpan')
        },
        success: function(res) {
          const response = res.data
          DOCUMENT_URL = response.document_url
          FULL_DOCUMENT_URL = response.full_document_url
          endLoadingButton('#upload-document-button', 'Simpan')
          showSuccess(res.message)
          renderDocument()
          $('#upload-document-modal').modal('hide')
        }
    });
  });

  //submit form
  $('#event-schedule-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#event-schedule-create-button')
    
    let $form = $(this)
    let data = {
      description:  $form.find( "textarea[name='description']" ).val(),
      event_id: $('#event-schedule-event-create-field').find(":selected").val(),
      pdf_url: DOCUMENT_URL,
    }
    $.ajax({
        async: true,
        url: SCHEDULE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#event-schedule-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#event-schedule-create-button', 'Simpan')
          showSuccess(res.message)
          $('#event-schedule-create-modal').modal('hide')
          schedule_table.ajax.reload()
        }
    });
  })
  
  $('#event-schedule-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#event-schedule-update-button')

    let $form = $(this)
    let data = {
      id: SCHEDULE_ID,
      description:  $form.find( "textarea[name='description']" ).val(),
      event_id: $('#event-schedule-event-update-field').find(":selected").val(),
      pdf_url: DOCUMENT_URL,
    }
    console.log("REQUEST: ", data)
    $.ajax({
        async: true,
        url: SCHEDULE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#event-schedule-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#event-schedule-update-button', 'Simpan')
          showSuccess(res.message)
          $('#event-schedule-update-modal').modal('toggle')
          schedule_table.ajax.reload()
        }
    });
  })

  $('#event-schedule-delete-button').click(function (){
    startLoadingButton('#event-schedule-delete-button')
    
    $.ajax({
        async: true,
        url: `${SCHEDULE_API_URL}${SCHEDULE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#event-schedule-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#event-schedule-delete-button', 'Iya')
          showSuccess(res.message)
          $('#event-schedule-delete-modal').modal('toggle')
          schedule_table.ajax.reload()
        }
    });
  })
})

function get_event(){
  $.ajax({
    async: true,
    url: `${SCHEDULE_API_URL}event`,
    type: 'GET',
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      renderEvent(res.data)
    }
  });
}

function renderEvent(events){
  let optionsHtml = ""
  events.forEach(item => {
    let itemHtml = `<option value="${item.event_id}">${item.event_nama}</option>`
    optionsHtml += itemHtml
  });
  $('.event-options').html(optionsHtml)
}

function renderForm(data, type){
  let $form = $(`#event-schedule-${type}-form`)
  $form.find( "textarea[name='description']" ).val(data.description)
  $(`#event-schedule-event-${type}-field`).val(data.event_id).change()
}

function clearForm(type){
  let $form = $(`#event-schedule-${type}-form`)
  $form.find( "textarea[name='description']" ).val("")
}

function renderDocument(){
  let img_html = `<a href="${FULL_DOCUMENT_URL}" target="_blank">${FULL_DOCUMENT_URL}</a>`
  $('.display-pdf').html(img_html)
}
