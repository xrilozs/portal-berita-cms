$(document).ready(function(){
  // if(ADMIN_ROLE == 'ADMIN') window.location.href = 'outbounds'
  // if(ADMIN_ROLE == 'SALES') window.location.href = 'sales-books'
  
  get_news_total()
  get_document_total()
  get_photo_total()
  get_video_total()
});

function get_news_total(){
  $.ajax({
    async: true,
    url: `${NEWS_API_URL}count?status=PUBLISH`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-news').html(response)
    }
  });
}

function get_document_total(){
  $.ajax({
    async: true,
    url: `${DOCUMENT_API_URL}/count?status=PUBLISH`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      console.log("RES: ", res.responseText)
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-document').html(response)
    }
  });
}

function get_photo_total(){
  $.ajax({
    async: true,
    url: `${PHOTO_API_URL}/count?status=PUBLISH`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-photo').html(response)
    }
  });
}

function get_video_total(){
  $.ajax({
    async: true,
    url: `${VIDEO_API_URL}count?status=PUBLISH`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-video').html(response)
    }
  });
}