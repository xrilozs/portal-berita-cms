let LIVE_STREAMING_ID

$(document).ready(function(){

  //render datatable
  let live_streaming_table = $('#live-streaming-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: LIVE_STREAMING_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "title",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "alias",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          {
            data: "video_url",
            render: function (data, type, row, meta) {
              let url = `https://youtube.com/watch?v=${data}`
              return `<a href="${url}" target="_blank">${url}</a>`
            },
          },
          { 
            data: "status",
            orderable: false,
            render: function (data, type, row, meta) {
              let color = data == 'DRAFT' ? 'default' : 'primary'
              let badge = `<span class="badge badge-pill badge-${color}">${data}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return formatDateID(data)
            },
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">`
                
                actions += `<button class="btn btn-sm btn-success live-streaming-update-toggle" data-id="${data}" data-toggle="modal" data-target="#live-streaming-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger live-streaming-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#live-streaming-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`

                if(row.status == 'DRAFT'){
                  actions += `<button class="btn btn-sm btn-primary live-streaming-publish-toggle" data-id="${data}" data-toggle="modal" data-target="#live-streaming-publish-modal" title="publikasi">
                    <i class="fas fa-check"></i>
                  </button>`
                }else{
                  actions += `<button class="btn btn-sm btn-warning live-streaming-unpublish-toggle" data-id="${data}" data-toggle="modal" data-target="#live-streaming-unpublish-modal" title="draft">
                    <i class="fas fa-ban"></i>
                  </button>`
                }

                actions += '</div>'

                return actions
            }
          }
      ]
  });
  
  //button action click
  $("#live-streaming-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".live-streaming-update-toggle", "click", function(e) {
    LIVE_STREAMING_ID = $(this).data('id')
    clearForm('update')

    $('#live-streaming-update-overlay').show()
    $.ajax({
        async: true,
        url: `${LIVE_STREAMING_API_URL}by-id/${LIVE_STREAMING_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#live-streaming-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#live-streaming-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".live-streaming-delete-toggle", "click", function(e) {
    LIVE_STREAMING_ID = $(this).data('id')
    console.log("ID:", LIVE_STREAMING_ID)
  })

  $("body").delegate(".live-streaming-publish-toggle", "click", function(e) {
    LIVE_STREAMING_ID = $(this).data('id')
    console.log("ID:", LIVE_STREAMING_ID)
  })

  $("body").delegate(".live-streaming-unpublish-toggle", "click", function(e) {
    LIVE_STREAMING_ID = $(this).data('id')
    console.log("ID:", LIVE_STREAMING_ID)
  })

  //submit form
  $('#live-streaming-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#live-streaming-create-button')
    
    let $form = $(this)
    let data = {
      title:  $form.find( "input[name='title']" ).val(),
      status: $('#live-streaming-status-create-field').find(":selected").val(),
      video_url: $form.find( "input[name='video_url']" ).val(),
    }
    $.ajax({
        async: true,
        url: LIVE_STREAMING_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#live-streaming-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#live-streaming-create-button', 'Simpan')
          showSuccess(res.message)
          $('#live-streaming-create-modal').modal('hide')
          live_streaming_table.ajax.reload()
        }
    });
  })
  
  $('#live-streaming-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#live-streaming-update-button')

    let $form = $(this)
    let data = {
      id: LIVE_STREAMING_ID,
      title:  $form.find( "input[name='title']" ).val(),
      video_url: $form.find( "input[name='video_url']" ).val(),
    }
    console.log("REQUEST: ", data)
    $.ajax({
        async: true,
        url: LIVE_STREAMING_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#live-streaming-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#live-streaming-update-button', 'Simpan')
          showSuccess(res.message)
          $('#live-streaming-update-modal').modal('toggle')
          live_streaming_table.ajax.reload()
        }
    });
  })
  
  $('#live-streaming-delete-button').click(function (){
    startLoadingButton('#live-streaming-delete-button')
    
    $.ajax({
        async: true,
        url: `${LIVE_STREAMING_API_URL}${LIVE_STREAMING_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#live-streaming-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#live-streaming-delete-button', 'Iya')
          showSuccess(res.message)
          $('#live-streaming-delete-modal').modal('toggle')
          live_streaming_table.ajax.reload()
        }
    });
  })

  $('#live-streaming-publish-button').click(function (){
    startLoadingButton('#live-streaming-publish-button')
    
    $.ajax({
        async: true,
        url: `${LIVE_STREAMING_API_URL}publish/${LIVE_STREAMING_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#live-streaming-publish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#live-streaming-publish-button', 'Iya')
          showSuccess(res.message)
          $('#live-streaming-publish-modal').modal('toggle')
          live_streaming_table.ajax.reload()
        }
    });
  })

  $('#live-streaming-unpublish-button').click(function (){
    startLoadingButton('#live-streaming-unpublish-button')
    
    $.ajax({
        async: true,
        url: `${LIVE_STREAMING_API_URL}unpublish/${LIVE_STREAMING_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#live-streaming-unpublish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#live-streaming-unpublish-button', 'Iya')
          showSuccess(res.message)
          $('#live-streaming-unpublish-modal').modal('toggle')
          live_streaming_table.ajax.reload()
        }
    });
  })

  $('#live-streaming-title-update-field').change(function(){
    let val = $(this).val()
    val = val.trim()
    let alias = val.toLowerCase().replaceAll(/\s+/g, "-")
    $('#live-streaming-alias-update-field').val(alias)
  })  
})

function renderForm(data, type){
  let $form = $(`#live-streaming-${type}-form`)
  $form.find( "input[name='title']" ).val(data.title)
  $form.find( "input[name='video_url']" ).val(data.video_url)
  $(`#live-streaming-status-${type}-field`).val(data.status).change()
  if(type == "update"){
    $form.find( "input[name='alias']" ).val(data.alias)
  }
}

function clearForm(type){
  let $form = $(`#live-streaming-${type}-form`)
  $form.find( "input[name='title']" ).val("")
  $form.find( "input[name='video_url']" ).val("")
}
