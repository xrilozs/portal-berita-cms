let API_URL               = sessionStorage.getItem("api-url")
let WEB_URL               = sessionStorage.getItem("cms-url")
let ADMIN_API_URL         = `${API_URL}admin/`
let APPS_API_URL          = `${API_URL}apps/`
let CATEGORY_NEWS_API_URL = `${API_URL}category_news/`
let COMPANY_API_URL       = `${API_URL}company/`
let CONFIG_API_URL        = `${API_URL}config/`
let DOCUMENT_API_URL      = `${API_URL}document/`
let PHOTO_API_URL         = `${API_URL}photo/`
let VIDEO_API_URL         = `${API_URL}video/`
let NEWS_API_URL          = `${API_URL}news/`
let PROFILE_API_URL       = `${API_URL}profile/`
let LIVE_STREAMING_API_URL= `${API_URL}live_streaming/`
let SCHEDULE_API_URL      = `${API_URL}competition_schedule/`
let TOAST                 = Swal.mixin({
                              toast: true,
                              position: 'top-end',
                              showConfirmButton: false,
                              timer: 3000
                            });
let SESSION               = sessionStorage.getItem("user-token")
let REFRESH_SESSION       = sessionStorage.getItem("user-refresh-token")
let ADMIN_ROLE            = sessionStorage.getItem("user-role")
let ADMIN_FULLNAME        = sessionStorage.getItem("user-fullname")
let RETRY_COUNT           = 0
let ADMIN_PAGES           = ['Apps', 'Category News', 'Gallery Photo', 'Gallery Video', 'News', 'Profile', 'Document', 'Dashboard', 'Live Streaming', 'Event Schedule', 'Event Schedule Daily']
let CURRENT_PAGE          = sessionStorage.getItem("current-page")
let DATATABLE_LANGUAGE    = {
  lengthMenu: "_MENU_ data per halaman",
  zeroRecords: "Data tidak ditemukan",
  search: "Cari",
  info: "Halaman _PAGE_ dari _PAGES_",
  infoEmpty: "Belum ada data",
  infoFiltered: "(filtered from _MAX_ total records)",
  loadingRecords: "Memuat...",
  processing: "Memuat",
  paginate: {
    next: "Selanjutnya",
    previous: "Sebelumnya"
  }
}

$('.rich-text-create').summernote({
  placeholder: 'Type here..',
  height: 300
});
$('.rich-text-update').summernote({
  placeholder: 'Type here..',
  height: 300
});

$('.select2').select2()
$('.datepicker').datepicker({
  format: 'dd-mm-yyyy',
  orientation: 'bottom',
  autoclose: true
})

$('.timepicker').datetimepicker({
  format: 'HH:mm',
})

if($('.datepicker').val()==""){
  $('.datepicker').datepicker("setDate", new Date());
}

$(document).ready(function(){
  if(!SESSION){
    window.location.href = `${WEB_URL}login`
  }else{
    checkAuthorityPage()
  }
  
  renderHeader()
  renderSidebar()
  getConfig()

  $('#password-visibility').click(function(){
    var x = document.getElementById("old-password");
    var y = document.getElementById("new-password");
    if (x.type === "password") {
      x.type = "text";
      y.type = "text";
      $('#visibility-checkbox').prop('checked', true); // Checks it
    } else {
      x.type = "password";
      y.type = "password";
      $('#visibility-checkbox').prop('checked', false); // Checks it
    }
  })
});

function renderHeader(){
  $('#header-adminName').html(`${ADMIN_FULLNAME} [${ADMIN_ROLE}]&nbsp;&nbsp;<i class="fas fa-user"></i>`)
}

function renderSidebar(){
  if(ADMIN_ROLE == 'SUPERADMIN'){
    $(".hidden-menu").css("display", "block")
  }
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function showSuccessV2(title, text, confirmLink=null){
  Swal.fire({
      icon: 'success',
      title: title,
      text: text,
      confirmButtonColor: '#00923F'
  }).then((result) => {
      if (confirmLink) {
        window.location.href = confirmLink
      }
  }); 
}

function setSession(data){
  console.log("SET NEW SESSION")
  sessionStorage.setItem("user-token", data.access_token)
  sessionStorage.setItem("user-refresh-token", data.refresh_token)
  sessionStorage.setItem("user-fullname", data.fullname);
  sessionStorage.setItem("user-role", data.role);
  SESSION = data.access_token
  REFRESH_SESSION = data.refresh_token
}

function removeSession(){
  console.log("REMOVE SESSION")
  sessionStorage.removeItem("user-token");
  sessionStorage.removeItem("user-refresh-token");
  sessionStorage.removeItem("user-fullname");
  sessionStorage.removeItem("user-role");
  window.location.href = `${WEB_URL}login`
}

function refreshToken(){
  let resp = {}
  $.ajax({
      async: false,
      url: `${API_URL}admin/refresh`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${REFRESH_SESSION}`);
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        resp = {status: "failed"}
      },
      success: function(res) {
        const response = res.data
        resp = {status: "success", data: response}
      }
  });
  
  return resp
}

function retryRequest(responseError){
  console.log("RETRY: ", RETRY_COUNT)
  if(responseError.code == 401){
    if(RETRY_COUNT < 3){
      let resObj = refreshToken()          
      if(resObj.status == 'success'){
        RETRY_COUNT += 1 
        setSession(resObj.data)
        return true
      }else if(resObj.status == 'failed'){
        removeSession()
      }
    }else{
      removeSession()
    }
  }else{
    showError(responseError.message)
    return false
  }
}

function checkAuthorityPage(){
  if(ADMIN_ROLE != 'SUPERADMIN'){
    let pages = ADMIN_PAGES
    if(!pages.includes(CURRENT_PAGE)){
      window.location.href = WEB_URL + `dashboard`
    }
  }
}

function formatRupiah(angka, prefix){
  var angkaStr  = angka.replace(/[^,\d]/g, '').toString(),
      split     = angkaStr.split(','),
      sisa      = split[0].length % 3,
      rupiah    = split[0].substr(0, sisa),
      ribuan    = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah    += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function formatDate(datetimeStr){
  let datetimeArr = datetimeStr.split(" ")
  let dateStr     = datetimeArr[0]
  let arr         = dateStr.split("-")
  return `${arr[2]}-${arr[1]}-${arr[0]}`
}

function formatDateID(dateStr=null, isTime=false){

  let day_arr = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
  let month_arr = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']

  let datetime  = dateStr ? new Date(dateStr) : new Date(),
      day       = day_arr[datetime.getDay()],
      date      = datetime.getDate(),
      month     = month_arr[datetime.getMonth()],
      year      = datetime.getFullYear()
  if(isTime){
    let hour    = datetime.getHours(),
        minute  = datetime.getMinutes()

    return `${day}, ${date} ${month} ${year} ${hour}:${minute}`
  }else{
    return `${day}, ${date} ${month} ${year}`
  }

}

function sortText(text){
  return text.length > 20 ? text.slice(0, 20) + ".." : text
}

$("#logout-button").click(function(){    
  removeSession()
})

$("#changePassword-toggle").click(function(e){
  let $form = $("#changePassword-form" )
  $form.find( "input[name='oldPassword']" ).val('')
  $form.find( "input[name='newPassword']" ).val('')
})

$("#changePassword-form").submit(function(e){
  e.preventDefault()
  startLoadingButton("#changePassword-button")

  let $form = $( this ),
      oldPassword = $form.find( "input[name='oldPassword']" ).val(),
      newPassword = $form.find( "input[name='newPassword']" ).val()
  
  $.ajax({
      async: true,
      url: `${API_URL}admin/change-password`,
      type: 'PUT',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify({
        old_password: oldPassword, 
        new_password: newPassword
      }),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        showError(response.message)
        endLoadingButton('#changePassword-button', 'Submit')
        let is_retry = retryRequest(response)
        if(is_retry) $.ajax(this)
      },
      success: function(res) {
        showSuccess(res.message)
        endLoadingButton('#changePassword-button', 'Submit')
        $('#changePassword-modal').modal('hide')
      }
  });
})

$(document).on('keydown', '.input-decimal', function(e){
  console.log("input decimal")
  let input   = $(this);
  let oldVal  = input.val();
  let regex
  let attr = input.attr('pattern')
  if (typeof attr !== 'undefined' && attr !== false) {
    regex = new RegExp(input.attr('pattern'), 'g');
  }else{
    regex = new RegExp(/^\s*-?(\d+(\.\d{1,2})?|\.\d{1,2})\s*$/, 'g')
  }
  
  setTimeout(function(){
    var newVal = input.val();
    if(!regex.test(newVal)){
      input.val(oldVal); 
    }
  }, 1);
});

$(document).on('change', '.input-currency', function(e){
  var oldVal = $(this).val();
  $(this).val(formatRupiah(oldVal))
});

function getConfig(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      let config = res.data
      if(config){
        if(config.title_1){
          $('#brand-name').html(config.title_1)
        }
        if(config.logo_1_url){
          $('.brand-image').prop("src", API_URL+config.logo_1_url)
        }
        if(config.icon_url){
          $("#favicon").attr("href", API_URL+config.icon_url);
        }
      }
    }
  });
}