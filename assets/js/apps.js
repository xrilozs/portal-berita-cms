let APPS_ID,
    IMG_URL,
    FULL_IMG_URL

$(document).ready(function(){
  //render datatable
  let apps_table = $('#apps-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: APPS_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          {
            data: "full_icon_url",
            className: "dt-body-center",
            render: function (data, type, row, meta) {
                return `<img src="${data}" class="img-fluid" style="max-width:100px;">`
            },
          },
          { 
            data: "name",
          },
          { 
            data: "url",
            render: function(data){
              return `<a href="${data}" target="_blank">${data}</a>`
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">`

                actions += `<button class="btn btn-sm btn-success apps-update-toggle" data-id="${data}" data-toggle="modal" data-target="#apps-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger apps-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#apps-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`

                actions += '</div>'

                return actions
            },
          }
      ]
  });
  
  //upload image
  $('#apps-uploadImage-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#apps-uploadImage-button')
    
    $.ajax({
        async: true,
        url: `${APPS_API_URL}upload-icon`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#apps-uploadImage-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#apps-uploadImage-button', 'Upload')
        },
        success: function(res) {
          const response = res.data
          IMG_URL = response.img_url
          FULL_IMG_URL = response.full_img_url
          endLoadingButton('#apps-uploadImage-button', 'Upload')
          showSuccess(res.message)
          renderImage()
          $('#apps-uploadImage-modal').modal('hide')
        }
    });
  });
  
  //button action click
  $("#apps-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".apps-update-toggle", "click", function(e) {
    APPS_ID = $(this).data('id')
    clearForm('update')

    $('#apps-update-overlay').show()
    $.ajax({
        async: true,
        url: `${APPS_API_URL}by-id/${APPS_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#apps-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          IMG_URL = response.icon_url
          FULL_IMG_URL = response.full_icon_url
          $('#apps-update-overlay').hide()
          renderImage()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".apps-delete-toggle", "click", function(e) {
    APPS_ID = $(this).data('id')
    console.log("ID:", APPS_ID)
  })

  //submit form
  $('#apps-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#apps-create-button')
    
    let $form = $(this)
    let data = {
      name:  $form.find( "input[name='name']" ).val(),
      url:  $form.find( "input[name='url']" ).val(),
      icon_url: IMG_URL,
    }
    $.ajax({
        async: true,
        url: APPS_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#apps-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#apps-create-button', 'Simpan')
          showSuccess(res.message)
          $('#apps-create-modal').modal('hide')
          apps_table.ajax.reload()
        }
    });
  })
  
  $('#apps-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#apps-update-button')

    let $form = $(this)
    let data = {
      id: APPS_ID,
      name:  $form.find( "input[name='name']" ).val(),
      url:  $form.find( "input[name='url']" ).val(),
      icon_url: IMG_URL,
    }

    $.ajax({
        async: true,
        url: APPS_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#apps-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#apps-update-button', 'Simpan')
          showSuccess(res.message)
          $('#apps-update-modal').modal('toggle')
          apps_table.ajax.reload()
        }
    });
  })
  
  $('#apps-delete-button').click(function (){
    startLoadingButton('#apps-delete-button')
    
    $.ajax({
        async: true,
        url: `${APPS_API_URL}${APPS_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#apps-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#apps-delete-button', 'Iya')
          showSuccess(res.message)
          $('#apps-delete-modal').modal('toggle')
          apps_table.ajax.reload()
        }
    });
  })
})

function renderImage(){
  let img_html = `<img src="${FULL_IMG_URL}" class="img-fluid" style="max-width:100px;">`
  $('.apps-displayImage').html(img_html)
}

function renderForm(data, type){
  let $form = $(`#apps-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  $form.find( "input[name='url']" ).val(data.url)
}

function clearForm(type){
  let $form = $(`#apps-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  $form.find( "input[name='url']" ).val("")
  $('.apps-displayImage').html('')
}
