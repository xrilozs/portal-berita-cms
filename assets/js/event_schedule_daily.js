let SCHEDULE_ID,
    SCHEDULE_ITEM_ID,
    DOCUMENT_URL,
    FULL_DOCUMENT_URL,
    RUNDOWN_TOTAL = 1

$(document).ready(function(){
  let event_schedule_option = $('#event_schedule_option').select2({
    placeholder: "Ketik nama event..",
    ajax: {
      async: true,
      url: `${SCHEDULE_API_URL}`,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      type: "GET",
      data: function (params) {
        var query = {
          search: params.term,
          page_number: 0,
          page_size: 10
        }
  
        return query;
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      processResults: function (res) {
        console.log(res.data)
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: res.data.map(item => {
            return {
              id: item.id,
              text: `${item.event_nama} - ${item.description}`
            }
          })
        };
      }
    }
  });

  event_schedule_option.data('select2').$selection.css('height', '34px');

  //render datatable
  let schedule_table = $('#event-schedule-item-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: `${SCHEDULE_API_URL}items/${SCHEDULE_ID}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "event_nama",
            render: function (data, type, row, meta) {
              return `${data} - ${row.description}`
            },
          },
          {
            data: "date",
            render: function(data, type, row){
              return `${formatDateID(data)}`
            }
          },

          {
            data: "schedule_json",
            render: function(data){
              return sortText(data)
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">
                  <button class="btn btn-sm btn-success event-schedule-item-update-toggle" data-id="${data}" data-toggle="modal" data-target="#event-schedule-item-update-modal" title="update">
                    <i class="fas fa-edit"></i>
                  </button>
                  <button class="btn btn-sm btn-danger event-schedule-item-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#event-schedule-item-delete-modal" title="hapus">
                    <i class="fas fa-trash"></i>
                  </button>
                </div>`

                return actions
            }
          }
      ]
  });

  event_schedule_option.on("select2:selecting", function(e) { 
    SCHEDULE_ID = e.params.args.data.id
    console.log("SELECTED:", SCHEDULE_ID)
    $('#items-section').show()
    schedule_table.ajax.url(`${SCHEDULE_API_URL}items/${SCHEDULE_ID}`).load();
    schedule_table.draw()
  });
  
  $("body").delegate(".event-schedule-item-delete-toggle", "click", function(e) {
    SCHEDULE_ITEM_ID = $(this).data('id')
    console.log("ID:", SCHEDULE_ITEM_ID)
  })

  $("body").delegate(".event-schedule-item-update-toggle", "click", function(e) {
    SCHEDULE_ITEM_ID = $(this).data('id')
    console.log("ID:", SCHEDULE_ITEM_ID)

    $('#other-rundown-create').html("")
    $('#other-rundown-update').html("")

    $('#event-schedule-item-update-overlay').show()
    $.ajax({
        async: true,
        url: `${SCHEDULE_API_URL}item/${SCHEDULE_ID}/${SCHEDULE_ITEM_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#event-schedule-item-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#event-schedule-item-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })

  $("#event-schedule-item-create-toggle").click(function(){
    RUNDOWN_TOTAL = 1
    $('#other-rundown-create').html("")
    $('#other-rundown-update').html("")
  })

  $('#add-rundown-create').click(function(){
    RUNDOWN_TOTAL += 1
    console.log("ELEMENT ID: ", RUNDOWN_TOTAL)
    insertRundownCreate(RUNDOWN_TOTAL)
  })

  $('#add-rundown-update').click(function(){
    RUNDOWN_TOTAL += 1
    console.log("ELEMENT ID: ", RUNDOWN_TOTAL)
    insertRundownUpdate(null, RUNDOWN_TOTAL)
  })

  $("body").delegate(".remove-rundown-button", "click", function(e) {
    let elementId = $(this).data('id')
    console.log("REMOVE ID:", elementId)
    $(`#rundown-${elementId}-form`).remove();
  })

  //submit form
  $('#event-schedule-item-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#event-schedule-item-create-button')
    
    let $form = $(this)
    let items = []
    for(let i=1; i<=RUNDOWN_TOTAL; i++){
      if($(`#rundown-${i}-form`).length){
        let start_hour = $form.find(`input[name='timepicker-s-${i}']`).val()
        let end_hour = $form.find(`input[name='timepicker-e-${i}']`).val()
        items.push({
          hour: `${start_hour} - ${end_hour}`, 
          title: $form.find(`input[name='title-${i}']`).val(), 
          description: $form.find(`textarea[name='description-${i}']`).val()
        })
      }
    }
    let data = {
      competition_schedule_id: SCHEDULE_ID,
      date: formatDate($form.find( "input[name='date']" ).val()),
      schedule_json: JSON.stringify(items)
    }
    console.log("DATA: ", data)
    $.ajax({
        async: true,
        url: `${SCHEDULE_API_URL}item`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#event-schedule-item-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#event-schedule-item-create-button', 'Simpan')
          showSuccess(res.message)
          $('#event-schedule-item-create-modal').modal('hide')
          schedule_table.draw()
        }
    });
  })
  
  $('#event-schedule-item-update-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#event-schedule-item-update-button')
    
    let $form = $(this)
    let items = []
    for(let i=2; i<=RUNDOWN_TOTAL; i++){
      if($(`#rundown-${i}-form`).length){
        let start_hour = $form.find(`input[name='timepicker-s-${i}']`).val()
        let end_hour = $form.find(`input[name='timepicker-e-${i}']`).val()
        items.push({
          hour: `${start_hour} - ${end_hour}`, 
          title: $form.find(`input[name='title-${i}']`).val(), 
          description: $form.find(`textarea[name='description-${i}']`).val()
        })
      }
    }
    let data = {
      id: SCHEDULE_ITEM_ID,
      competition_schedule_id: SCHEDULE_ID,
      date: formatDate($form.find( "input[name='date']" ).val()),
      schedule_json: JSON.stringify(items)
    }
    console.log("DATA: ", data)
    $.ajax({
        async: true,
        url: `${SCHEDULE_API_URL}item`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#event-schedule-item-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#event-schedule-item-update-button', 'Simpan')
          showSuccess(res.message)
          $('#event-schedule-item-update-modal').modal('hide')
          schedule_table.draw()
        }
    });
  })

  $('#event-schedule-item-delete-button').click(function (){
    startLoadingButton('#event-schedule-item-delete-button')
    
    $.ajax({
        async: true,
        url: `${SCHEDULE_API_URL}item/${SCHEDULE_ITEM_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#event-schedule-item-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#event-schedule-item-delete-button', 'Iya')
          showSuccess(res.message)
          $('#event-schedule-item-delete-modal').modal('toggle')
          schedule_table.ajax.reload()
        }
    });
  })
})

function insertRundownCreate(elementId){
  $('#other-rundown-create').append(`<div class="card" id="rundown-${elementId}-form">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            <label>Jam Mulai:</label>
            <div class="input-group" id="timepicker-s-${elementId}" data-target-input="nearest">
              <input type="text" name="timepicker-s-${elementId}" class="form-control datetimepicker-input" data-target="#timepicker-s-${elementId}"/>
              <div class="input-group-append" data-target="#timepicker-s-${elementId}" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="far fa-clock"></i></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            <label>Jam Selesai:</label>
            <div class="input-group" id="timepicker-e-${elementId}" data-target-input="nearest">
              <input type="text" name="timepicker-e-${elementId}" class="form-control datetimepicker-input" data-target="#timepicker-e-${elementId}"/>
              <div class="input-group-append" data-target="#timepicker-e-${elementId}" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="far fa-clock"></i></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <label>Judul Kegiatan</label>
          <input type="text" name="title-${elementId}" class="form-control" id="title-${elementId}">
        </div>
        <div class="col-lg-12">
          <label>Deskripsi Kegiatan</label>
          <textarea name="description-${elementId}" class="form-control" id="description-${elementId}"></textarea>
        </div>
      </div>
    </div>
    <div class="card-footer text-right font-weight-bold p-3">
      <button type="button" class="btn btn-danger remove-rundown-button" data-id="${elementId}">Hapus</button>
    </div>
  </div>`);
  $(`#timepicker-s-${elementId}`).datetimepicker({
    format: 'HH:mm',
  })
  $(`#timepicker-e-${elementId}`).datetimepicker({
    format: 'HH:mm',
  })
}

function insertRundownUpdate(data, elementId){
  let hour = data ? data.hour : null
  let hour_arr = hour ? hour.split(" - ") : null
  let start_hour = hour_arr ? hour_arr[0] : '' 
  let end_hour = hour ? hour_arr[1] : ''
  let title = data ? data.title : ''
  let description = data ? data.description : ''
  $('#other-rundown-update').append(`<div class="card" id="rundown-${elementId}-form">
    <div class="card-body">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            <label>Jam Mulai:</label>
            <div class="input-group" id="timepicker-s-${elementId}" data-target-input="nearest">
              <input type="text" name="timepicker-s-${elementId}" class="form-control datetimepicker-input" data-target="#timepicker-s-${elementId}" value="${start_hour}"/>
              <div class="input-group-append" data-target="#timepicker-s-${elementId}" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="far fa-clock"></i></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="form-group">
            <label>Jam Selesai:</label>
            <div class="input-group" id="timepicker-e-${elementId}" data-target-input="nearest">
              <input type="text" name="timepicker-e-${elementId}" class="form-control datetimepicker-input" data-target="#timepicker-e-${elementId}" value="${end_hour}"/>
              <div class="input-group-append" data-target="#timepicker-e-${elementId}" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="far fa-clock"></i></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <label>Judul Kegiatan</label>
          <input type="text" name="title-${elementId}" class="form-control" id="title-${elementId}" value="${title}">
        </div>
        <div class="col-lg-12">
          <label>Deskripsi Kegiatan</label>
          <textarea name="description-${elementId}" class="form-control" id="description-${elementId}">${description}</textarea>
        </div>
      </div>
    </div>
    <div class="card-footer text-right font-weight-bold p-3">
      <button type="button" class="btn btn-danger remove-rundown-button" data-id="${elementId}">Hapus</button>
    </div>
  </div>`);
  $(`#timepicker-s-${elementId}`).datetimepicker({
    format: 'HH:mm',
  })
  $(`#timepicker-e-${elementId}`).datetimepicker({
    format: 'HH:mm',
  })
}

function renderEvent(events){
  let optionsHtml = ""
  events.forEach(item => {
    let itemHtml = `<option value="${item.event_id}">${item.event_nama}</option>`
    optionsHtml += itemHtml
  });
  $('.event-options').html(optionsHtml)
}

function renderForm(data, type){
  let $form = $(`#event-schedule-item-${type}-form`)
  $form.find( "input[name='date']" ).val(data.date)
  let items = JSON.parse(data.schedule_json)
  RUNDOWN_TOTAL = items.length + 1
  items.forEach((item, index) => {
    insertRundownUpdate(item, index+2)
  });
}

