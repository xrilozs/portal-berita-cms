let CATEGORY_NEWS_ID,
    IMG_URL,
    THUMBNAIL_URL,
    FULL_THUMBNAIL_URL

$(document).ready(function(){
  //render datatable
  let category_news_table = $('#category-news-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: CATEGORY_NEWS_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name",
          },
          { 
            data: "alias",
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          // {
          //   data: "id",
          //   className: "dt-body-right",
          //   render: function (data, type, row, meta) {
          //       return `<button class="btn btn-sm btn-primary blog-category-update-toggle" data-id="${data}" data-toggle="modal" data-target="#blog-category-update-modal" title="update">
          //         <i class="fas fa-edit"></i>
          //       </button>
          //       <button class="btn btn-sm btn-danger blog-category-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#blog-category-delete-modal" title="delete">
          //         <i class="fas fa-trash"></i>
          //       </button>`
          //   },
          // }
      ]
  });
  
  //button action click
  $("#blog-category-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".blog-category-update-toggle", "click", function(e) {
    CATEGORY_NEWS_ID = $(this).data('id')
    clearForm('update')

    $('#blog-category-update-overlay').show()
    $.ajax({
        async: true,
        url: `${CATEGORY_NEWS_API_URL}by-id/${CATEGORY_NEWS_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#blog-category-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#blog-category-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".blog-category-delete-toggle", "click", function(e) {
    CATEGORY_NEWS_ID = $(this).data('id')
    console.log("ID:", CATEGORY_NEWS_ID)
  })

  $('#blog-category-name-update-field').change(function(){
    let val = $(this).val()
    val = val.trim()
    let alias = val.toLowerCase().replace(" ", "-")
    $('#blog-category-alias-update-field').val(alias)
  })

  //submit form
  $('#blog-category-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#blog-category-create-button')
    
    let $form = $(this)
    let data = {
      name:  $form.find( "input[name='name']" ).val(),
    }
    $.ajax({
        async: true,
        url: CATEGORY_NEWS_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#blog-category-create-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#blog-category-create-button', 'Submit')
          showSuccess(res.message)
          $('#blog-category-create-modal').modal('hide')
          category_news_table.ajax.reload()
        }
    });
  })
  
  $('#blog-category-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#blog-category-update-button')

    let $form = $(this)
    let data = {
      id: CATEGORY_NEWS_ID,
      name:  $form.find( "input[name='name']" ).val()
    }

    $.ajax({
        async: true,
        url: CATEGORY_NEWS_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#blog-category-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#blog-category-update-button', 'Submit')
          showSuccess(res.message)
          $('#blog-category-update-modal').modal('toggle')
          category_news_table.ajax.reload()
        }
    });
  })
  
  $('#blog-category-delete-button').click(function (){
    startLoadingButton('#blog-category-delete-button')
    
    $.ajax({
        async: true,
        url: `${CATEGORY_NEWS_API_URL}${CATEGORY_NEWS_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#blog-category-delete-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#blog-category-delete-button', 'Yes')
          showSuccess(res.message)
          $('#blog-category-delete-modal').modal('toggle')
          category_news_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#blog-category-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
  if(type == 'update'){
    $form.find( "input[name='alias']" ).val(data.alias)
  }
}

function clearForm(type){
  let $form = $(`#blog-category-${type}-form`)
  $form.find( "input[name='name']" ).val("")
  if(type == 'update'){
    $form.find( "input[name='alias']" ).val("")
  }
}
