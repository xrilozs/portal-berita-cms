let THUMBNAIL_URL = null,
    LIST_PHOTO = []

$(document).ready(function(){
  $('#thumbnail-photo-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#thumbnail-photo-button')
    
    $.ajax({
        async: true,
        url: `${PHOTO_API_URL}upload-image`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#thumbnail-photo-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#thumbnail-photo-button', 'Simpan')
        },
        success: function(res) {
          const response = res.data
          THUMBNAIL_URL = response.img_url
          endLoadingButton('#thumbnail-photo-button', 'Simpan')
          $('#thumbnail-photo-modal').modal('hide')
          showSuccess(res.message)
          render_thumbnail()
        }
    });
  });

  $('#add-photo-form').submit(function(e){
    e.preventDefault()
    startLoadingButton('#add-photo-button')
    
    $.ajax({
        async: true,
        url: `${PHOTO_API_URL}upload-image`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: new FormData($('#add-photo-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#add-photo-button', 'OK')
        },
        success: function(res) {
          endLoadingButton('#add-photo-button', 'OK')
          $('#add-photo-modal').modal('hide')

          showSuccess(res.message)
          LIST_PHOTO.push(res.data.img_url)
          render_photos()
        }
    });
  });

  $('#create-photo-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#create-photo-button')

    let isValidThumbnail  = THUMBNAIL_URL ? true : false
    let isValidPhotos     = LIST_PHOTO.length > 0 ? true : false

    if(!isValidThumbnail){
      showError('Thumbnail belum diisi')
      endLoadingButton('#create-photo-button', 'Simpan')
    }else if(!isValidPhotos){
      showError('Foto belum diisi')
      endLoadingButton('#create-photo-button', 'Simpan')
    }else{
      let $form = $(this)
      let data = {
        title:  $form.find( "input[name='title']" ).val(),
        description: $form.find( "textarea[name='description']" ).val(),
        status: $('#photo-status-create-field').find(":selected").val(),
        thumbnail_url: THUMBNAIL_URL,
        event_date: $form.find( "input[name='event_date']" ).val()
      }
      $.ajax({
          async: true,
          url: PHOTO_API_URL,
          type: 'POST',
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          data: JSON.stringify(data),
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
            else endLoadingButton('#create-photo-button', 'Simpan')
          },
          success: function(res) {
            upload_photo_items(res.data.id)
          }
      });
    }
  })

  $("body").delegate(".delete-photo", "click", function(e) {
    let index = $(this).data('id')
    console.log(index)
    LIST_PHOTO.splice(index, 1)
    render_photos()
  })
})

function upload_photo_items(gallery_photo_id){
  LIST_PHOTO.forEach(item => {
    let data = {
      gallery_photo_id: gallery_photo_id,
      photo_url: item
    }

    $.ajax({
      async: false,
      url: `${PHOTO_API_URL}item`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify(data),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        // else endLoadingButton('#create-photo-button', 'Simpen')
      },
      success: function(res) {
      }
    });

    endLoadingButton('#create-photo-button', 'Simpan')
    showSuccessV2("Create Photo", "Create Photo Success", `${WEB_URL}gallery-photo`)
  })
}

function render_thumbnail(){
  let img_html = `<img src="${API_URL}${THUMBNAIL_URL}" class="img-fluid" style="max-width:100px;">`
  $('.photo-display-thumbnail').html(img_html)
}

function render_photos(){
  let photos_html = ""
  LIST_PHOTO.forEach((item, i) => {
    const item_html = `<div class="col-lg-2 col-md-3 col-sm-6 mb-2">
      <div class="row">
        <div class="col-12 mb-4 d-flex justify-content-center">
          <img src="${API_URL}${item}" class="img-fluid" style="max-width:100px;">
        </div>
        <div class="col-12 d-flex justify-content-center">
          <button class="btn btn-sm btn-danger delete-photo" data-id="${i}">
            <i class="fas fa-trash"></i> Hapus
          </button>
        </div>
      </div>
    </div>`
    photos_html += item_html
  });

  $('#list-photos').html(photos_html)
}
