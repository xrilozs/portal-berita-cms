let VIDEO_ID

$(document).ready(function(){

  //render datatable
  let video_table = $('#video-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: VIDEO_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "title",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "alias",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          {
            data: "video_url",
            render: function (data, type, row, meta) {
              let url = `https://youtube.com/watch?v=${data}`
              return `<a href="${url}" target="_blank">${url}</a>`
            },
          },
          {
            data: "description",
            render: function (data, type, row, meta) {
              return sortText(data)
            },
          },
          { 
            data: "status",
            orderable: false,
            render: function (data, type, row, meta) {
              let color = data == 'DRAFT' ? 'default' : 'primary'
              let badge = `<span class="badge badge-pill badge-${color}">${data}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            render: function (data, type, row, meta) {
              return formatDateID(data)
            },
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">`
                
                actions += `<button class="btn btn-sm btn-success video-update-toggle" data-id="${data}" data-toggle="modal" data-target="#video-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger video-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#video-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`

                if(row.status == 'DRAFT'){
                  actions += `<button class="btn btn-sm btn-primary video-publish-toggle" data-id="${data}" data-toggle="modal" data-target="#video-publish-modal" title="publikasi">
                    <i class="fas fa-check"></i>
                  </button>`
                }else{
                  actions += `<button class="btn btn-sm btn-warning video-unpublish-toggle" data-id="${data}" data-toggle="modal" data-target="#video-unpublish-modal" title="draft">
                    <i class="fas fa-ban"></i>
                  </button>`
                }

                actions += '</div>'

                return actions
            }
          }
      ]
  });
  
  //button action click
  $("#video-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".video-update-toggle", "click", function(e) {
    VIDEO_ID = $(this).data('id')
    clearForm('update')

    $('#video-update-overlay').show()
    $.ajax({
        async: true,
        url: `${VIDEO_API_URL}by-id/${VIDEO_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#video-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#video-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".video-delete-toggle", "click", function(e) {
    VIDEO_ID = $(this).data('id')
    console.log("ID:", VIDEO_ID)
  })

  $("body").delegate(".video-publish-toggle", "click", function(e) {
    VIDEO_ID = $(this).data('id')
    console.log("ID:", VIDEO_ID)
  })

  $("body").delegate(".video-unpublish-toggle", "click", function(e) {
    VIDEO_ID = $(this).data('id')
    console.log("ID:", VIDEO_ID)
  })

  //submit form
  $('#video-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#video-create-button')
    
    let $form = $(this)
    let data = {
      title:  $form.find( "input[name='title']" ).val(),
      status: $('#video-status-create-field').find(":selected").val(),
      video_url: $form.find( "input[name='video_url']" ).val(),
      description: $form.find( "textarea[name='description']" ).val(),
    }
    $.ajax({
        async: true,
        url: VIDEO_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#video-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#video-create-button', 'Simpan')
          showSuccess(res.message)
          $('#video-create-modal').modal('hide')
          video_table.ajax.reload()
        }
    });
  })
  
  $('#video-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#video-update-button')

    let $form = $(this)
    let data = {
      id: VIDEO_ID,
      title:  $form.find( "input[name='title']" ).val(),
      status: $('#video-status-update-field').find(":selected").val(),
      video_url: $form.find( "input[name='video_url']" ).val(),
      description: $form.find( "textarea[name='description']" ).val(),
    }
    console.log("REQUEST: ", data)
    $.ajax({
        async: true,
        url: VIDEO_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#video-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#video-update-button', 'Simpan')
          showSuccess(res.message)
          $('#video-update-modal').modal('toggle')
          video_table.ajax.reload()
        }
    });
  })
  
  $('#video-delete-button').click(function (){
    startLoadingButton('#video-delete-button')
    
    $.ajax({
        async: true,
        url: `${VIDEO_API_URL}${VIDEO_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#video-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#video-delete-button', 'Iya')
          showSuccess(res.message)
          $('#video-delete-modal').modal('toggle')
          video_table.ajax.reload()
        }
    });
  })

  $('#video-publish-button').click(function (){
    startLoadingButton('#video-publish-button')
    
    $.ajax({
        async: true,
        url: `${VIDEO_API_URL}publish/${VIDEO_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#video-publish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#video-publish-button', 'Iya')
          showSuccess(res.message)
          $('#video-publish-modal').modal('toggle')
          video_table.ajax.reload()
        }
    });
  })

  $('#video-unpublish-button').click(function (){
    startLoadingButton('#video-unpublish-button')
    
    $.ajax({
        async: true,
        url: `${VIDEO_API_URL}unpublish/${VIDEO_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#video-unpublish-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#video-unpublish-button', 'Iya')
          showSuccess(res.message)
          $('#video-unpublish-modal').modal('toggle')
          video_table.ajax.reload()
        }
    });
  })

  $('#video-title-update-field').change(function(){
    let val = $(this).val()
    val = val.trim()
    let alias = val.toLowerCase().replace(" ", "-")
    $('#video-alias-update-field').val(alias)
  })  
})

function renderForm(data, type){
  let $form = $(`#video-${type}-form`)
  $form.find( "input[name='title']" ).val(data.title)
  $form.find( "input[name='video_url']" ).val(data.video_url)
  $(`#video-status-${type}-field`).val(data.status).change()
  if(type == "update"){
    $form.find( "input[name='alias']" ).val(data.alias)
  }
  $form.find( "textarea[name='description']" ).val(data.description)
}

function clearForm(type){
  let $form = $(`#video-${type}-form`)
  $form.find( "input[name='title']" ).val("")
  $form.find( "input[name='video_url']" ).val("")
  $form.find( "textarea[name='description']" ).val("")
}
