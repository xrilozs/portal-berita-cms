let USERADMIN_ID
$(document).ready(function(){
  //data table
  let userAdmin_table = $('#admin-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: `${ADMIN_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "fullname",
          },
          { 
            data: "username",
          },
          { 
            data: "role",
          },
          {
            data: "is_active",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'primary' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NON-AKTIF'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<div class="btn-group" role="group" aria-label="Basic example">`

              button += `<button class="btn btn-sm btn-success admin-update-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>`
              if(parseInt(row.is_active)){
                if(row.role != 'SUPERADMIN'){
                  button += ` <button class="btn btn-sm btn-danger admin-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-inactive-modal" title="non-aktif">
                    <i class="fas fa-times"></i>
                  </button>`
                }
              }else{
                  button += ` <button class="btn btn-sm btn-info admin-active-toggle" data-id="${data}" data-toggle="modal" data-target="#admin-active-modal" title="aktif">
                    <i class="fas fa-check"></i>
                  </button>`
              }

              button += '</div>'
              
              return button
            },
          }
      ]
  });
  
  //toggle
  $('#admin-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".admin-update-toggle", "click", function(e) {
    USERADMIN_ID = $(this).data('id')
    $('#admin-update-overlay').show()

    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}/by-id/${USERADMIN_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#admin-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#admin-update-overlay').hide()
        }
    });
  })
  
  $("body").delegate(".admin-inactive-toggle", "click", function(e) {
    USERADMIN_ID = $(this).data('id')
  })
  
  $("body").delegate(".admin-active-toggle", "click", function(e) {
    USERADMIN_ID = $(this).data('id')
  })

  //form
  $('#admin-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#admin-create-button')
    
    let $form = $(this),
        request = {
          fullname: $form.find( "input[name='fullname']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
          password: $form.find( "input[name='password']" ).val(),
          role: $('#admin-role-create-option').find(":selected").val()
        }
        
    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-create-button', 'Simpan')
          $('#admin-create-modal').modal('hide')
          userAdmin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#admin-update-button')

    let $form = $(this),
        roleBefore = $form.find( "input[name='role-hide']" ).val(),
        roleNow = roleBefore == 'SUPERADMIN' ? 'SUPERADMIN' : $('#admin-role-update-option').find(":selected").val()
        request = {
          id: USERADMIN_ID,
          fullname: $form.find( "input[name='fullname']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
          role: roleNow
        }

    $.ajax({
        async: true,
        url: ADMIN_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-update-button', 'Simpan')
          $('#admin-update-modal').modal('toggle')
          userAdmin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-inactive-button').click(function (){
    startLoadingButton('#admin-inactive-button')
    
    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}inactive/${USERADMIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-inactive-button', 'Iya')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-inactive-button', 'Iya')
          $('#admin-inactive-modal').modal('toggle')
          userAdmin_table.ajax.reload()
        }
    });
  })
  
  $('#admin-active-button').click(function (){
    startLoadingButton('#admin-active-button')
    
    $.ajax({
        async: true,
        url: `${ADMIN_API_URL}active/${USERADMIN_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#admin-active-button', 'Iya')
        },
        success: function(res) {
          showSuccess(res.message)
          endLoadingButton('#admin-active-button', 'Iya')
          $('#admin-active-modal').modal('toggle')
          userAdmin_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#admin-${type}-form`)
  $form.find( "input[name='fullname']" ).val(data.fullname)
  $form.find( "input[name='username']" ).val(data.username)
  if(data.role == 'SUPERADMIN'){
    $('#role-update').hide()
    $('#role-update').hide()
    $form.find( "input[name='role-hide']" ).val('SUPERADMIN')
  }else{
    $('#role-update').show()
    $form.find( "input[name='role-hide']" ).val(data.role)
    $(`#admin-role-${type}-option`).val(data.role).change()
  }


  let color = data.is_active ? 'primary' : 'danger'
  let text = data.is_active ? 'AKTIF' : 'NON-AKTIF'
  let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`
  $(`#admin-status-${type}-field`).html(badge)
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#admin-${type}-form`)
  $form.find( "input[name='fullname']" ).val("")
  $form.find( "input[name='username']" ).val("")
  if(type=='create') $form.find( "input[name='password']" ).val("")
}