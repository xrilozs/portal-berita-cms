let PROFILE_ID

$(document).ready(function(){
  //render datatable
  let profile_table = $('#profile-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ordering: false,
      language: DATATABLE_LANGUAGE,
      ajax: {
        async: true,
        url: PROFILE_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "title",
            render: function(data){
              return sortText(data)
            }
          },
          { 
            data: "alias",
            render: function(data){
              return sortText(data)
            }
          },
          { 
            data: "content",
            render: function(data){
              data = data.replace(/<[^>]+>/g, '');
              return sortText(data)
            }
          },
          {
            data: "created_at",
            render: function(data){
              return formatDateID(data)
            }
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
                let actions = `<div class="btn-group" role="group" aria-label="Basic example">`

                actions += `<button class="btn btn-sm btn-success profile-update-toggle" data-id="${data}" data-toggle="modal" data-target="#profile-update-modal" title="update">
                  <i class="fas fa-edit"></i>
                </button>
                <button class="btn btn-sm btn-danger profile-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#profile-delete-modal" title="hapus">
                  <i class="fas fa-trash"></i>
                </button>`

                actions += '</div>'

                return actions
            },
          }
      ]
  });
  
  //button action click
  $("#profile-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".profile-update-toggle", "click", function(e) {
    PROFILE_ID = $(this).data('id')
    clearForm('update')

    $('#profile-update-overlay').show()
    $.ajax({
        async: true,
        url: `${PROFILE_API_URL}by-id/${PROFILE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#profile-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#profile-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".profile-delete-toggle", "click", function(e) {
    PROFILE_ID = $(this).data('id')
    console.log("ID:", PROFILE_ID)
  })

  //submit form
  $('#profile-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#profile-create-button')
    
    let $form = $(this)
    let data = {
      title:  $form.find( "input[name='title']" ).val(),
      content: $('.rich-text-create').summernote('code')
    }
    $.ajax({
        async: true,
        url: PROFILE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#profile-create-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#profile-create-button', 'Submit')
          showSuccess(res.message)
          $('#profile-create-modal').modal('hide')
          profile_table.ajax.reload()
        }
    });
  })
  
  $('#profile-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#profile-update-button')

    let $form = $(this)
    let data = {
      id: PROFILE_ID,
      title:  $form.find( "input[name='title']" ).val(),
      content:  $('.rich-text-update').summernote('code')
    }
    console.log("REQUEST: ", data)
    $.ajax({
        async: true,
        url: PROFILE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#profile-update-button', 'Submit')
        },
        success: function(res) {
          endLoadingButton('#profile-update-button', 'Submit')
          showSuccess(res.message)
          $('#profile-update-modal').modal('toggle')
          profile_table.ajax.reload()
        }
    });
  })
  
  $('#profile-delete-button').click(function (){
    startLoadingButton('#profile-delete-button')
    
    $.ajax({
        async: true,
        url: `${PROFILE_API_URL}${PROFILE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#profile-delete-button', 'Yes')
        },
        success: function(res) {
          endLoadingButton('#profile-delete-button', 'Yes')
          showSuccess(res.message)
          $('#profile-delete-modal').modal('toggle')
          profile_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#profile-${type}-form`)
  $form.find( "input[name='title']" ).val(data.title)
  $(`.rich-text-${type}`).summernote("code", data.content)
  if(type == "update"){
    $form.find( "input[name='alias']" ).val(data.alias)
  }
}

function clearForm(type){
  let $form = $(`#profile-${type}-form`)
  $form.find( "input[name='title']" ).val("")
  $(`.rich-text-${type}`).summernote("code", "")
}
