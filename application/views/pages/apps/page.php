<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Aplikasi</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Aplikasi</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="apps-create-toggle" data-toggle="modal" data-target="#apps-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="apps-datatable">
                  <thead>
                    <tr>
                      <th>Gambar</th>
                      <th>Nama</th>
                      <th>URL</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="apps-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Apps</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="apps-create-form">
        <div class="form-group">
          <label for="apps-image-create-field">Gambar:</label>
          <p class="apps-displayImage">
          </p>
          <button type="button" class="btn btn-primary" id="apps-upload-image" data-toggle="modal" data-target="#apps-uploadImage-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Gambar
          </button>
        </div>
        <div class="form-group">
          <label for="apps-name-create-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="apps-name-create-field" placeholder="nama apps.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="apps-url-create-field">URL:</label>
          <input type="url" name="url" class="form-control" id="apps-url-create-field" placeholder="URL apps.." autocomplete="off" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="apps-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="apps-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="apps-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Apps</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="apps-update-form">          
        <div class="form-group">
          <label for="apps-image-update-field">Gambar:</label>
          <p class="apps-displayImage">
          </p>
          <button type="button" class="btn btn-primary" id="apps-upload-image" data-toggle="modal" data-target="#apps-uploadImage-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Gambar
          </button>
        </div>
        <div class="form-group">
          <label for="apps-name-update-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="apps-name-update-field" placeholder="nama apps.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="apps-url-update-field">URL:</label>
          <input type="url" name="url" class="form-control" id="apps-url-update-field" placeholder="URL apps.." autocomplete="off" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="apps-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="apps-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Apps</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus apps ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="apps-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="apps-uploadImage-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Gambar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="apps-uploadImage-form">          
        <div class="form-group">
          <label for="apps-uploadImage-field">Gambar:</label>
          <input type="file" class="form-control-file" id="apps-uploadImage-field" name="icon" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="apps-uploadImage-button">Upload</button>
        </form>
      </div>
    </div>
  </div>
</div>
