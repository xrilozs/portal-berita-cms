<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Report | User Top Affiliate</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">Report</li>
            <li class="breadcrumb-item active">User Top Affiliate</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="user-datatable">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Total Affiliate</th>
                      <th>Total Commission</th>
                      <th>Created At</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="user-detail-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="overlay" id="user-detail-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Detail User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="user-detail-form">
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="user-firstName-detail-field">First Name:</label>
              <input type="text" name="firstName" class="form-control" id="user-firstName-detail-field" readonly>
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="user-lastName-detail-field">Last Name:</label>
              <input type="text" name="lastName" class="form-control" id="user-lastName-detail-field" readonly>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="user-email-detail-field">Email:</label>
          <input type="email" name="email" class="form-control" id="user-email-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="user-phone-detail-field">Phone:</label>
          <input type="text" name="phone" class="form-control" id="user-phone-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="user-income-detail-field">Income:</label>
          <input type="text" name="income" class="form-control" id="user-income-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="user-balance-detail-field">Balance:</label>
          <input type="text" name="balance" class="form-control" id="user-balance-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="user-uniqueCode-detail-field">Unique Code:</label>
          <input type="text" name="uniqueCode" class="form-control" id="user-uniqueCode-detail-field" readonly>
        </div>
        <div class="form-group">
          <label for="user-affiliate-detail-field">Affiliate:</label>
          <input type="text" name="affiliate" class="form-control" id="user-affiliate-detail-field" readonly>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>
