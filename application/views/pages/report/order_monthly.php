<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Report | Order Monthly</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">Report</li>
            <li class="breadcrumb-item active">Order Monthly</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">

              <div class="row">
                <div class="col-12 d-flex justify-content-end mb-1">
                  <div class="form-group col-6">
                    <label>Date</label>
                    <input type="text" class="form-control annual-picker" id="date" placeholder="-Select date-">
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="order-datatable">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Order Qty</th>
                      <th>Order Total</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
