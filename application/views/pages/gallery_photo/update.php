<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-12">
          <h1>Update Galeri Foto</h1>
        </div>
        <div class="col-6">
          <a href="<?=base_url('gallery-photo');?>"> <i class="fas fa-chevron-circle-left"></i> Kembali</a>
        </div>
        <div class="col-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item">Galeri</li>
            <li class="breadcrumb-item"><a href="<?=base_url('gallery-photo');?>">Foto</a></li>
            <li class="breadcrumb-item active">Update</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
                <form id="update-photo-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label for="photo-thumbnail-update-field">Gambar:</label>
                      <p class="photo-display-thumbnail">
                      </p>
                      <button type="button" class="btn btn-primary" id="thumbnail-photo" data-toggle="modal" data-target="#thumbnail-photo-modal">
                        <i class="fas fa-cloud-upload-alt"></i> Upload Thumbnail
                      </button>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Judul</label>
                      <input type="text" name="title" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea name="description" class="form-control" cols="30" rows="5" required></textarea>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Tanggal Acara</label>
                      <input type="text" class="form-control datepicker" name="event_date" id="event_date">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label for="photo-status-update-field">Status:</label>
                      <select name="status" class="form-control" id="photo-status-update-field">
                        <option value="PUBLISH">PUBLISH</option>
                        <option value="DRAFT">DRAFT</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-12">
                    <button type="submit" class="btn btn-lg btn-success float-right" id="update-photo-button">
                      <i class="fas fa-save"></i> Simpan
                    </button>
                  </div>
                </div>
                </form>
              <hr>
              <div id="report-photo">
                <h1>Foto</h1>
                <div class="row mb-4">
                  <button type="button" class="btn btn-primary" id="add-photo-toggle" data-toggle="modal" data-target="#add-photo-modal">
                    <i class="fas fa-plus"></i> Tambah
                  </button>
                </div>
                <div class="row" id="list-photos"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="thumbnail-photo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Thumbnail</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="thumbnail-photo-form">          
        <div class="form-group">
          <label>Foto:</label>
          <input type="file" class="form-control-file" id="thumbnail-photo-field" name="image" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="thumbnail-photo-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="add-photo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Foto</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="add-photo-form">          
        <div class="form-group">
          <label>Foto:</label>
          <input type="file" class="form-control-file" id="add-photo-field" name="image" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="add-photo-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="delete-photo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Photo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus Foto ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="delete-photo-button">Iya</button>
      </div>
    </div>
  </div>
</div>
