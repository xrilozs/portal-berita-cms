<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pengaturan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Pengaturan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="overlay" id="contact-overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-address-book"></i> Kontak</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <form id="contact-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Google Maps Embed</label>
                      <textarea name="google_map" class="form-control" id="google_map" cols="30" rows="5"></textarea>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email.." required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Nomor Telepon</label>
                      <input type="text" class="form-control" id="phone" name="phone" placeholder="Nomor Telepon.." required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control" id="address" name="address" placeholder="Alamat.." required></textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-success float-right" id="contact-button">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="col-12">
          <div class="card">
            <div class="overlay" id="social-overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-mobile-alt"></i> Sosial Media</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <form id="social-form">
                <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                      <label>Facebook</label>
                      <input type="text" class="form-control" id="sm_facebook" name="sm_facebook" placeholder="Facebook..">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Twitter</label>
                      <input type="text" class="form-control" id="sm_twitter" name="sm_twitter" placeholder="Twitter..">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Instagram</label>
                      <input type="text" class="form-control" id="sm_instagram" name="sm_instagram" placeholder="Instagram..">
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                      <label>Youtube</label>
                      <input type="text" class="form-control" id="sm_youtube" name="sm_youtube" placeholder="Youtube..">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-success float-right" id="social-button">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="col-12">
          <div class="card">
            <div class="overlay" id="web-overlay">
              <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
            <div class="card-header">
              <h3 class="card-title font-weight-bold"><i class="fas fa-globe-asia"></i> Website</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <form id="web-form">
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label>Judul 1</label>
                      <input type="text" class="form-control" id="title_1" name="title_1" placeholder="Judul 1..">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label>Judul 2</label>
                      <input type="text" class="form-control" id="title_2" name="title_2" placeholder="Judul 2..">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label>Logo 1:</label>
                      <p id="config-display-logo-1"></p>
                      <button type="button" class="btn btn-primary config-upload-logo" data-id="1" data-toggle="modal" data-target="#config-upload-logo-modal">
                        <i class="fas fa-cloud-upload-alt"></i> Upload Logo
                      </button>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label>Logo 2:</label>
                      <p id="config-display-logo-2"></p>
                      <button type="button" class="btn btn-primary config-upload-logo" data-id="2" data-toggle="modal" data-target="#config-upload-logo-modal">
                        <i class="fas fa-cloud-upload-alt"></i> Upload Logo
                      </button>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label>Favicon:</label>
                      <p id="config-display-icon">
                      </p>
                      <button type="button" class="btn btn-primary" id="config-upload-icon" data-toggle="modal" data-target="#config-upload-icon-modal">
                        <i class="fas fa-cloud-upload-alt"></i> Upload Favicon
                      </button>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-success float-right" id="web-button">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="config-upload-logo-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Logo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="config-upload-logo-form">          
        <div class="form-group">
          <label for="config-upload-logo-field">Logo:</label>
          <input type="file" class="form-control-file" id="config-upload-logo-field" name="image" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="config-upload-logo-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="config-upload-icon-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Favicon</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="config-upload-icon-form">          
        <div class="form-group">
          <label for="config-upload-icon-field">Icon:</label>
          <input type="file" class="form-control-file" id="config-upload-icon-field" name="image" accept="image/ico" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="config-upload-icon-button">OK</button>
        </form>
      </div>
    </div>
  </div>
</div>
