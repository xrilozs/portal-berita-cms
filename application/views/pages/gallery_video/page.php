<!-- Content Wrapper. Contains video content -->
<div class="content-wrapper">
  <!-- Content Header (Video header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Galeri Video</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Galeri</li>
            <li class="breadcrumb-item active">Video</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="video-create-toggle" data-toggle="modal" data-target="#video-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="video-datatable">
                  <thead>
                    <tr>
                      <th>Judul</th>
                      <th>Alias</th>
                      <th>Link Video</th>
                      <th>Deskripsi</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="video-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="video-create-form">
        <div class="form-group">
          <label for="video-title-create-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="video-title-create-field" placeholder="video title.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="video-content-create-field">Link Video (Youtube):</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="video_url">https://youtube.com/watch?v=</span>
            </div>
            <input type="text" class="form-control" id="video_url" name="video_url">
          </div>
        </div>
        <div class="form-group">
          <label for="video-description-create-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="video-description-create-field" required></textarea>
        </div>
        <div class="form-group">
          <label for="video-status-create-field">Status:</label>
          <select name="status" class="form-control" id="video-status-create-field">
            <option value="PUBLISH">PUBLISH</option>
            <option value="DRAFT">DRAFT</option>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="video-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="video-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="video-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="video-update-form">
        <div class="form-group">
          <label for="video-name-update-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="video-title-update-field" placeholder="video title.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="video-alias-update-field">Alias:</label>
          <input type="text" name="alias" class="form-control" id="video-alias-update-field" placeholder="video alias.." disabled>
        </div>
        <div class="form-group">
          <label for="video-content-update-field">Link Video (Youtube):</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="video_url">https://youtube.com/watch?v=</span>
            </div>
            <input type="text" class="form-control" id="video_url" name="video_url">
          </div>
        </div>
        <div class="form-group">
          <label for="video-description-update-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="video-description-update-field" required></textarea>
        </div>
        <div class="form-group">
          <label for="video-status-update-field">Status:</label>
          <select name="status" class="form-control" id="video-status-update-field">
            <option value="PUBLISH">PUBLISH</option>
            <option value="DRAFT">DRAFT</option>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success" id="video-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="video-publish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Publikasi Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mempublikasi video ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="video-publish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="video-unpublish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Draft Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin men-draft video ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="video-unpublish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="video-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Video</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus video ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="video-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>
