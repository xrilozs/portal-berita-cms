<!-- Content Wrapper. Contains video content -->
<div class="content-wrapper">
  <!-- Content Header (Video header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Live Streaming</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Live Streaming</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="live-streaming-create-toggle" data-toggle="modal" data-target="#live-streaming-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="live-streaming-datatable">
                  <thead>
                    <tr>
                      <th>Judul</th>
                      <th>Alias</th>
                      <th>Link Video</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="live-streaming-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Live Streaming</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="live-streaming-create-form">
        <div class="form-group">
          <label for="live-streaming-title-create-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="live-streaming-title-create-field" placeholder="Judul Live Streaming.."  required>
        </div>
        <div class="form-group">
          <label for="live-streaming-content-create-field">Link Live Streaming (Youtube):</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="video_url">https://youtube.com/watch?v=</span>
            </div>
            <input type="text" class="form-control" id="video_url" name="video_url">
          </div>
        </div>
        <div class="form-group">
          <label for="live-streaming-status-create-field">Status:</label>
          <select name="status" class="form-control" id="live-streaming-status-create-field">
            <option value="PUBLISH">PUBLISH</option>
            <option value="DRAFT">DRAFT</option>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="live-streaming-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="live-streaming-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="live-streaming-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Live Streaming</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="live-streaming-update-form">
        <div class="form-group">
          <label for="live-streaming-title-update-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="live-streaming-title-update-field" placeholder="Judul Live Streaming.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="live-streaming-alias-update-field">Alias:</label>
          <input type="text" name="alias" class="form-control" id="live-streaming-alias-update-field" placeholder="Live Streaming alias.." disabled>
        </div>
        <div class="form-group">
          <label for="live-streaming-content-update-field">Link Video (Youtube):</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="video_url">https://youtube.com/watch?v=</span>
            </div>
            <input type="text" class="form-control" id="video_url" name="video_url">
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success" id="live-streaming-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="live-streaming-publish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Publikasi Live Streaming</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mempublikasi live streaming ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="live-streaming-publish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="live-streaming-unpublish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Draft Live Streaming</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin men-draft live streaming ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="live-streaming-unpublish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="live-streaming-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Live Streaming</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus live streaming ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="live-streaming-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>
