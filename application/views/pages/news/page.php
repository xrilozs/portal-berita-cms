<!-- Content Wrapper. Contains news content -->
<div class="content-wrapper">
  <!-- Content Header (Berita header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Berita</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Berita</li>
            <li class="breadcrumb-item active">Berita</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="news-create-toggle" data-toggle="modal" data-target="#news-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="news-datatable">
                  <thead>
                    <tr>
                      <th>Judul</th>
                      <th>Kategori</th>
                      <th>Alias</th>
                      <th>Gambar</th>
                      <th>Meta Description</th>
                      <th>Meta Keywords</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="news-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Buat Berita</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="news-create-form">
        <div class="form-group">
          <label for="news-image-create-field">Gambar:</label>
          <p class="news-displayImage">
          </p>
          <button type="button" class="btn btn-primary" id="news-upload-image" data-toggle="modal" data-target="#news-uploadImage-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Gambar
          </button>
        </div>
        <div class="form-group">
          <label for="news-title-create-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="news-title-create-field" placeholder="news title.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="news-category-create-field">Kategori:</label>
          <select name="category" class="form-control news-category-option" id="news-category-create-field">
          </select>
        </div>
        <div class="form-group">
          <label for="news-content-create-field">Konten:</label>
          <textarea name="content" class="form-control rich-text-create" id="news-content-create-field" required>
          </textarea>
        </div>
        <div class="form-group">
          <label for="news-meta-description-create-field">Meta Description (Optional):</label>
          <textarea name="meta_description" class="form-control" id="news-meta-description-create-field"></textarea>
        </div>
        <div class="form-group">
          <label for="news-meta-keywords-create-field">Meta Keywords (Optional):</label>
          <textarea name="meta_keywords" class="form-control" id="news-meta-keywords-create-field"></textarea>
        </div>
        <div class="form-group" id="news-status-create">
          <label for="news-status-create-field">Status:</label>
          <select name="status" class="form-control" id="news-status-create-field">
            <option value="DRAFT" selected>DRAFT</option>
            <option value="PUBLISH">PUBLISH</option>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="news-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="news-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="news-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update Berita</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="news-update-form">
        <div class="form-group">
          <label for="news-image-update-field">Gambar:</label>
          <p class="news-displayImage">
          </p>
          <button type="button" class="btn btn-primary news-upload-image" data-toggle="modal" data-target="#news-uploadImage-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Gambar
          </button>
        </div>
        <div class="form-group">
          <label for="news-name-update-field">Judul:</label>
          <input type="text" name="title" class="form-control" id="news-title-update-field" placeholder="news title.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="news-alias-update-field">Alias:</label>
          <input type="text" name="alias" class="form-control" id="news-alias-update-field" placeholder="news alias.." disabled>
        </div>
        <div class="form-group">
          <label for="news-category-update-field">Kategori:</label>
          <select name="category" class="form-control news-category-option" id="news-category-update-field">
          </select>
        </div>
        <div class="form-group">
          <label for="news-content-update-field">Konten:</label>
          <textarea name="content" class="form-control rich-text-update" id="news-content-update-field" required>
          </textarea>
        </div>
        <div class="form-group">
          <label for="news-meta-description-update-field">Meta Description (Optional):</label>
          <textarea name="meta_description" class="form-control" id="news-meta-description-update-field"></textarea>
        </div>
        <div class="form-group">
          <label for="news-meta-keywords-update-field">Meta Keywords (Optional):</label>
          <textarea name="meta_keywords" class="form-control" id="news-meta-keywords-update-field"></textarea>
        </div>
        <div class="form-group">
          <label for="news-status-update-field">Status:</label>
          <select name="status" class="form-control" id="news-status-update-field">
            <option value="PUBLISH">PUBLISH</option>
            <option value="DRAFT">DRAFT</option>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="news-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="news-publish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Publikasi Berita</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin publikasi berita ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="news-publish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="news-unpublish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Draft Berita</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin men-draft berita ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="news-unpublish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="news-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Berita</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus berita ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="news-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="news-uploadImage-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Gambar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="news-uploadImage-form">          
        <div class="form-group">
          <label for="news-uploadImage-field">Gambar:</label>
          <input type="file" class="form-control-file" id="news-uploadImage-field" name="image" accept="image/*" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="news-uploadImage-button">Upload</button>
        </form>
      </div>
    </div>
  </div>
</div>
