<!-- Content Wrapper. Contains photo content -->
<div class="content-wrapper">
  <!-- Content Header (Foto header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Jadwal Perlombaan Harian</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item">Jadwal Perlombaan</li>
            <li class="breadcrumb-item active">Harian</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="form-group">
                <label for="event_schedule_option">Jadwal Perlombaan Umum</label>
                <select name="event_schedule" class="form-control" id="event_schedule_option"></select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row" id="items-section" style="display:none;">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row mb-3">
                <div class="col-12 d-flex justify-content-end">
                  <button class="btn btn-success" id="event-schedule-item-create-toggle" data-toggle="modal" data-target="#event-schedule-item-create-modal">
                    <i class="fas fa-plus"></i> Tambah
                  </button>
                </div>
              </div>
              <div class="table-responsive" >
                <table class="table table-bordered table-hover" id="event-schedule-item-datatable" style="width:100%">
                  <thead>
                    <tr>
                      <th>Deskripsi Jadwal Perlombaan</th>
                      <th>Tanggal</th>
                      <th>Jadwal (JSON)</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="event-schedule-item-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Tambah Jadwal harian</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="event-schedule-item-create-form">
        <div class="form-group">
          <label for="event-schedule-item-date-create-field">Tanggal:</label>
          <input type="text" class="form-control datepicker" name="date" id="date" required>
        </div>
        <div class="form-group">
          <label>Run Down:</label>
          <div class="row">
            <div class="card" id="rundown-1-form">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                      <label>Jam Mulai:</label>
                      <div class="input-group timepicker" id="timepicker-s-1" data-target-input="nearest">
                        <input type="text" name="timepicker-s-1" class="form-control datetimepicker-input" data-target="#timepicker-s-1" required/>
                        <div class="input-group-append" data-target="#timepicker-s-1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                      <label>Jam Selesai:</label>
                      <div class="input-group timepicker" id="timepicker-e-1" data-target-input="nearest">
                        <input type="text" name="timepicker-e-1" class="form-control datetimepicker-input" data-target="#timepicker-e-1" required/>
                        <div class="input-group-append" data-target="#timepicker-e-1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <label>Judul Kegiatan</label>
                    <input type="text" name="title-1" class="form-control" id="title-1" required>
                  </div>
                  <div class="col-lg-12">
                    <label>Deskripsi Kegiatan</label>
                    <textarea name="description-1" class="form-control" id="description-1" required></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div id="other-rundown-create"></div>
            <div class="col-lg-12 text-center">
              <button class="btn btn-success" type="button" id="add-rundown-create">
                <i class="fa fa-plus"></i> Tambah
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="event-schedule-item-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-schedule-item-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Tambah Jadwal harian</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="event-schedule-item-update-form">
        <div class="form-group">
          <label for="event-schedule-item-date-update-field">Tanggal:</label>
          <input type="text" class="form-control datepicker" name="date" id="date" required>
        </div>
        <div class="form-group">
          <label>Run Down:</label>
          <div class="row">
            <div id="other-rundown-update"></div>
            <div class="col-lg-12 text-center">
              <button class="btn btn-success" type="button" id="add-rundown-update">
                <i class="fa fa-plus"></i> Tambah
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="event-schedule-item-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-schedule-item-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Jadwal Perlombaan Harian</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus jadwal perlombaan harian ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="event-schedule-item-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>
