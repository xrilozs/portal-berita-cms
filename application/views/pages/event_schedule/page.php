<!-- Content Wrapper. Contains photo content -->
<div class="content-wrapper">
  <!-- Content Header (Foto header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Jadwal Perlombaan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Jadwal Perlombaan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button class="btn btn-success" id="event-schedule-create-toggle" data-toggle="modal" data-target="#event-schedule-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="event-schedule-datatable">
                  <thead>
                    <tr>
                      <th>Nama Acara</th>
                      <th>Deskripsi</th>
                      <th>Status Acara</th>
                      <th>Tanggal Acara</th>
                      <th>PDF</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="event-schedule-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Buat Jadwal Perlombaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="event-schedule-create-form">
        <div class="form-group">
          <label>Dokumen:</label>
          <p class="display-pdf"></p>
          <button type="button" class="btn btn-primary" id="upload-pdf-toggle" data-toggle="modal" data-target="#upload-document-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Dokumen
          </button>
        </div>
        <div class="form-group">
          <label for="event-schedule-event-create-field">Event Perlombaan:</label>
          <select name="event" class="form-control event-options" id="event-schedule-event-create-field"></select>
        </div>
        <div class="form-group">
          <label for="event-schedule-description-create-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="event-schedule-description-create-field" cols="30" rows="10" placeholder="Deskripsi.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="event-schedule-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-schedule-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Update Jadwal Perlombaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="event-schedule-update-form">
        <div class="form-group">
          <label>Dokumen:</label>
          <p class="display-pdf"></p>
          <button type="button" class="btn btn-primary" id="upload-pdf-toggle" data-toggle="modal" data-target="#upload-document-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Dokumen
          </button>
        </div>
        <div class="form-group">
          <label for="event-schedule-event-update-field">Event Perlombaan:</label>
          <select name="event" class="form-control event-options" id="event-schedule-event-update-field"></select>
        </div>
        <div class="form-group">
          <label for="event-schedule-description-update-field">Deskripsi:</label>
          <textarea name="description" class="form-control" id="event-schedule-description-update-field" cols="30" rows="10" placeholder="Deskripsi.." required></textarea>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="event-schedule-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="event-schedule-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Foto</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus jadwal perlombaan ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="event-schedule-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="upload-document-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Upload Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="upload-document-form">          
        <div class="form-group">
          <label for="upload-document-field">Dokumen:</label>
          <input type="file" class="form-control-file" id="upload-document-field" name="doc" accept="application/pdf" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="upload-document-button">Upload</button>
        </form>
      </div>
    </div>
  </div>
</div>
