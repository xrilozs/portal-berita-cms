<!-- Content Wrapper. Contains document content -->
<div class="content-wrapper">
  <!-- Content Header (Dokumen header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Dokumen</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Dokumen</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="document-create-toggle" data-toggle="modal" data-target="#document-create-modal">
                  <i class="fas fa-plus"></i> Buat
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="document-datatable">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Alias</th>
                      <th>Dokumen</th>
                      <th>Ukuran</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="document-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Buat Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="document-create-form">
        <div class="form-group">
          <label>Dokumen:</label>
          <p class="display-document"></p>
          <button type="button" class="btn btn-primary" id="upload-document-toggle" data-toggle="modal" data-target="#upload-document-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Dokumen
          </button>
        </div>
        <div class="form-group">
          <label for="document-name-create-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="document-name-create-field" placeholder="Nama.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="document-status-create-field">Status:</label>
          <select name="status" class="form-control" id="document-status-create-field">
            <option value="PUBLISH">PUBLISH</option>
            <option value="DRAFT">DRAFT</option>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="document-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="document-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="document-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-name">Update Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="document-update-form">
        <div class="form-group">
          <label>Dokumen:</label>
          <p class="display-document"></p>
          <button type="button" class="btn btn-primary upload-document-toggle" data-toggle="modal" data-target="#upload-document-modal">
            <i class="fas fa-cloud-upload-alt"></i> Upload Dokumen
          </button>
        </div>
        <div class="form-group">
          <label for="document-name-update-field">Nama:</label>
          <input type="text" name="name" class="form-control" id="document-name-update-field" placeholder="Nama.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="document-status-update-field">Status:</label>
          <select name="status" class="form-control" id="document-status-update-field" disabled>
            <option value="PUBLISH">PUBLISH</option>
            <option value="DRAFT">DRAFT</option>
          </select>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="document-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="document-publish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Publikasi Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mempublikasi dokumen ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-primary" id="document-publish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="document-unpublish-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Draft Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin men-draft dokumen ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-warning" id="document-unpublish-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="document-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Hapus Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus dokumen ini?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="document-delete-button">Iya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="upload-document-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-name">Upload Dokumen</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="upload-document-form">          
        <div class="form-group">
          <label for="upload-document-field">Dokumen:</label>
          <input type="file" class="form-control-file" id="upload-document-field" name="doc" accept="application/pdf" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="upload-document-button">Upload</button>
        </form>
      </div>
    </div>
  </div>
</div>
