<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Kategori Berita</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=BASE_URL;?>dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Berita</li>
            <li class="breadcrumb-item active">Kategori</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <!-- <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-success" id="category-news-create-toggle" data-toggle="modal" data-target="#category-news-create-modal">
                  <i class="fas fa-plus"></i> Create
                </button>
              </div> -->
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="category-news-datatable">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Alias</th>
                      <th>Tanggal Dibuat</th>
                      <!-- <th>Action</th> -->
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="category-news-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Create News Category</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="category-news-create-form">
        <div class="form-group">
          <label for="category-news-name-create-field">Name:</label>
          <input type="text" name="name" class="form-control" id="category-news-name-create-field" placeholder="blog category name.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" id="category-news-create-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="category-news-update-modal"  data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="category-news-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update News Category</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="category-news-update-form">
        <div class="form-group">
          <label for="category-news-name-update-field">Name:</label>
          <input type="text" name="name" class="form-control" id="category-news-name-update-field" placeholder="blog category name.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="category-news-alias-update-field">Alias:</label>
          <input type="text" name="alias" class="form-control" id="category-news-alias-update-field" placeholder="blog category alias.." disabled>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" id="category-news-update-button">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="category-news-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete News Category</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure want to delete this News Category?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-danger" id="category-news-delete-button">Yes</button>
      </div>
    </div>
  </div>
</div>
