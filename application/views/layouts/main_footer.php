<footer class="main-footer" style="padding:12px; text-align:center;">
    <a href="<?=WEB_URL;?>"><?=COMPANY_NAME;?> </a><b>CMS Version</b> 1.0
    <p class="mb-0" style="font-size:9px;">
      <strong>Copyright &copy; 2014-2020 AdminLTE.io. All rights reserved.</strong>
    </p>
</footer>
<!-- /.control-sidebar -->

  <!-- Common Modal -->
  <div class="modal fade" id="changePassword-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Ganti Kata Sandi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="changePassword-form">
          <div class="form-group">
            <label>Kata Sandi Lama:</label>
            <input type="password" id="old-password" name="oldPassword" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Kata Sandi Baru:</label>
            <input type="password" id="new-password" name="newPassword" class="form-control" required>
          </div>
          <div class="form-group">
            <div id="password-visibility" style="cursor: pointer;">
              <input type="checkbox" id="visibility-checkbox"> Lihat Password
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success" id="changePassword-button">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/chart.js/Chart.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/jszip/jszip.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/moment/moment.min.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=ASSETS;?>third-party/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=ASSETS;?>third-party/adminlte/dist/js/adminlte.js"></script>
<!-- Select2 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/select2/js/select2.full.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/sweetalert2/sweetalert2.min.js"></script>

<!-- Vitalets Datepicker -->
<!-- <script src="<?=ASSETS;?>third-party/vitalets/js/bootstrap-datepicker.js"></script> -->
<!-- CodeMirror -->
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/codemirror.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/mode/css/css.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/mode/xml/xml.js"></script>
<script src="<?=ASSETS;?>third-party/adminlte/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<!-- timepicker -->
<script src="<?=ASSETS;?>third-party/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Custom JS -->
<script>
  sessionStorage.setItem("current-page", "<?=$page;?>");
  sessionStorage.setItem("api-url", "<?=API_URL;?>");
  sessionStorage.setItem("cms-url", "<?=BASE_URL;?>");
</script>
<script src="<?=ASSETS;?>js/common.js?v=<?=$version;?>"></script>
<?= $page=="Home" ? '<script src="'.ASSETS.'js/home.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Dashboard" ? '<script src="'.ASSETS.'js/dashboard.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Admin" ? '<script src="'.ASSETS.'js/admin.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="News" ? '<script src="'.ASSETS.'js/news.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Category News" ? '<script src="'.ASSETS.'js/category_news.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Apps" ? '<script src="'.ASSETS.'js/apps.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Config" ? '<script src="'.ASSETS.'js/config.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Gallery Photo" ? '<script src="'.ASSETS.'js/gallery_photo.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Create Gallery Photo" ? '<script src="'.ASSETS.'js/gallery_photo_create.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Update Gallery Photo" ? '<script src="'.ASSETS.'js/gallery_photo_update.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Gallery Video" ? '<script src="'.ASSETS.'js/gallery_video.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Profile" ? '<script src="'.ASSETS.'js/profile.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Document" ? '<script src="'.ASSETS.'js/document.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Event Schedule" ? '<script src="'.ASSETS.'js/event_schedule.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Event Schedule Daily" ? '<script src="'.ASSETS.'js/event_schedule_daily.js?v='.$version.'"></script>' : ''; ?>
<?= $page=="Live Streaming" ? '<script src="'.ASSETS.'js/live_streaming.js?v='.$version.'"></script>' : ''; ?>
</body>
</html>
