  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-1">
    <!-- Brand Logo -->
    <a href="<?=BASE_URL;?>dashboard" class="brand-link">
      <img src="<?=ASSETS;?>img/logo.png" alt="Logo" class="brand-image" style="opacity: .8">
      <span class="brand-text font-weight-bold" id="brand-name"><?=COMPANY_NAME;?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item" id="sidebar-dashboard-menu">
            <a href="<?=BASE_URL;?>dashboard" class="nav-link <?=$page == 'Dashboard' ? 'active' : '';?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item <?=$page == 'Category News' || $page == 'News' ? 'menu-is-opening menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-newspaper"></i>
              <p>
                Berita
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=BASE_URL;?>category-news" class="nav-link <?=$page == 'Category News' ? 'active' : '';?>">
                  <i class="fas fa-tags nav-icon"></i>
                  <p>Kategori Berita</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=BASE_URL;?>news" class="nav-link <?=$page == 'News' ? 'active' : '';?>">
                  <i class="fas fa-pen nav-icon"></i>
                  <p>Berita</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item <?=$page == 'Gallery Photo' || $page == 'Gallery Video' ? 'menu-is-opening menu-open' : '';?> ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-photo-video"></i>
              <p>
                Galeri
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=BASE_URL;?>gallery-photo" class="nav-link <?=$page == 'Gallery Photo' ? 'active' : '';?>">
                  <i class="fas fa-image nav-icon"></i>
                  <p>Galeri Foto</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=BASE_URL;?>gallery-video" class="nav-link <?=$page == 'Gallery Video' ? 'active' : '';?>">
                  <i class="fab fa-youtube nav-icon"></i>
                  <p>Galeri Video</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item" id="sidebar-company-menu">
            <a href="<?=BASE_URL;?>apps" class="nav-link <?=$page == 'Apps' ? 'active' : '';?>" >
              <i class="nav-icon fas fa-cubes"></i>
              <p>
                Aplikasi
              </p>
            </a>
          </li>
          <li class="nav-item " id="sidebar-page-menu">
            <a href="<?=BASE_URL;?>profile" class="nav-link <?=$page == 'Profile' ? 'active' : '';?>">
              <i class="nav-icon fas fa-scroll"></i>
              <p>
                Halaman Profil
              </p>
            </a>
          </li>
          <li class="nav-item " id="sidebar-page-menu">
            <a href="<?=BASE_URL;?>document" class="nav-link <?=$page == 'Document' ? 'active' : '';?>">
              <i class="nav-icon fas fa-file-pdf"></i>
              <p>
                Dokumen
              </p>
            </a>
          </li>
          <li class="nav-item " id="sidebar-page-menu">
            <a href="<?=BASE_URL;?>live-streaming" class="nav-link <?=$page == 'Live Streaming' ? 'active' : '';?>">
              <i class="nav-icon fas fa-video"></i>
              <p>
                Live Streaming
              </p>
            </a>
          </li>
          <li class="nav-item <?=$page == 'Event Schedule' || $page == 'Event Schedule Daily' ? 'menu-is-opening menu-open' : '';?> ">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                Jadwal Perlombaan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('event-schedule');?>" class="nav-link <?=$page == 'Event Schedule' ? 'active' : '';?>">
                  <i class="fas far fa-calendar-alt nav-icon"></i>
                  <p>Jadwal Umum</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('event-schedule/daily');?>" class="nav-link <?=$page == 'Event Schedule Daily' ? 'active' : '';?>">
                  <i class="fab far fa-calendar-check nav-icon"></i>
                  <p>Jadwal Harian</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item hidden-menu" id="sidebar-admin-menu">
            <a href="<?=BASE_URL;?>admin" class="nav-link <?=$page == 'Admin' ? 'active' : '';?>">
              <i class="nav-icon fas fa-users-cog"></i>
              <p>
                Admin
              </p>
            </a>
          </li>
          <li class="nav-item hidden-menu" id="sidebar-config-menu">
            <a href="<?=BASE_URL;?>config" class="nav-link <?=$page == 'Config' ? 'active' : '';?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
