<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_photo extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function page(){    
    $data['page'] = "Gallery Photo";
    $data['version'] = date(ASSET_VERSION);

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/gallery_photo/page');
		$this->load->view('layouts/main_footer', $data);
  }

  public function create(){    
    $data['page'] = "Create Gallery Photo";
    $data['version'] = date(ASSET_VERSION);

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/gallery_photo/create');
		$this->load->view('layouts/main_footer', $data);
  }

  public function update(){    
    $data['page'] = "Update Gallery Photo";
    $data['version'] = date(ASSET_VERSION);

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/gallery_photo/update');
		$this->load->view('layouts/main_footer', $data);
  }
}