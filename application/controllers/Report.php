<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function user_top_affiliate(){    
    $data['page'] = "Report Top Affiliate";
    $data['version'] = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/user_top_affiliate');
		$this->load->view('layouts/main_footer', $data);
  }

  public function order_daily(){    
    $data['page'] = "Report Order Daily";
    $data['version'] = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/order_daily');
		$this->load->view('layouts/main_footer', $data);
  }

  public function order_weekly(){    
    $data['page'] = "Report Order Weekly";
    $data['version'] = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/order_weekly');
		$this->load->view('layouts/main_footer', $data);
  }

  public function order_monthly(){    
    $data['page'] = "Report Order Monthly";
    $data['version'] = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/order_monthly');
		$this->load->view('layouts/main_footer', $data);
  }

  public function deposit_daily(){    
    $data['page'] = "Report Deposit Daily";
    $data['version'] = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/deposit_daily');
		$this->load->view('layouts/main_footer', $data);
  }

  public function deposit_weekly(){    
    $data['page'] = "Report Deposit Weekly";
    $data['version'] = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/deposit_weekly');
		$this->load->view('layouts/main_footer', $data);
  }

  public function deposit_monthly(){    
    $data['page'] = "Report Deposit Monthly";
    $data['version'] = date(ASSET_VERSION);
		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/report/deposit_monthly');
		$this->load->view('layouts/main_footer', $data);
  }
}