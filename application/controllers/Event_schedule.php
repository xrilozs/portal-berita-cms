<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_schedule extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function page(){    
    $data['page'] = "Event Schedule";
    $data['version'] = date(ASSET_VERSION);

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/event_schedule/page');
		$this->load->view('layouts/main_footer', $data);
  }

  public function daily(){    
    $data['page'] = "Event Schedule Daily";
    $data['version'] = date(ASSET_VERSION);

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/event_schedule/daily');
		$this->load->view('layouts/main_footer', $data);
  }
}