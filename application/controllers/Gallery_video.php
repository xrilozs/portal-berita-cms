<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_video extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }
  
  public function page(){    
    $data['page'] = "Gallery Video";
    $data['version'] = date(ASSET_VERSION);

		$this->load->view('layouts/main_header', $data);
		$this->load->view('layouts/main_sidebar', $data);
		$this->load->view('pages/gallery_video/page');
		$this->load->view('layouts/main_footer', $data);
  }
}